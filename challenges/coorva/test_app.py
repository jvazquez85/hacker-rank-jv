import unittest

from app import is_palindrome


class TestPalindrome(unittest.TestCase):
    def test_is_palindrome_detects_palindromes(self):
        cases = {"ana": True, "anana": True, "radar": True, "apple": False}

        for word, expected_result in cases.items():
            result = is_palindrome(word)
            msg = (
                f"Expected that {word} should give {expected_result}, but got {result}"
            )
            self.assertEqual(expected_result, result, msg)


if __name__ == "__main__":
    unittest.main()

from collections import deque
from typing import Callable, Dict, List, Optional


class AdjacencyMapHelper:
    def __init__(self, number_of_elements: Optional[int] = None):
        if number_of_elements:
            self.adjacency_map: Dict[int, List[int]] = {
                i: [] for i in range(1, number_of_elements + 1)
            }
        else:
            self.adjacency_map: Dict[int, List[int]] = {}

    def add_edge(self, start: int, end: int, undirected: bool = True):
        if start not in self.adjacency_map:
            self.adjacency_map[start] = []
        if end not in self.adjacency_map:
            self.adjacency_map[end] = []

        self.adjacency_map[start].append(end)
        if undirected:
            self.adjacency_map[end].append(start)

    def build_from_edges(self, edges: List[List[int]], undirected: bool = True):
        for start, end in edges:
            self.add_edge(start, end, undirected)

    def get_adjacency_map(self) -> Dict[int, List[int]]:
        return self.adjacency_map

    def dfs(self, node: int, visited: set, component: List[int]):
        stack = [node]
        while stack:
            current = stack.pop()
            if current not in visited:
                visited.add(current)
                component.append(current)
                for neighbor in self.adjacency_map.get(current, []):
                    if neighbor not in visited:
                        stack.append(neighbor)

    def bfs(self, node: int, visited: set, component: List[int]):
        queue = deque([node])
        while queue:
            current = queue.popleft()
            if current not in visited:
                visited.add(current)
                component.append(current)
                for neighbor in self.adjacency_map.get(current, []):
                    if neighbor not in visited:
                        queue.append(neighbor)

    def find_connected_components(self, method: str = "dfs") -> List[List[int]]:
        visited = set()
        components = []

        if method == "dfs":
            traverse: Callable[[int, set, List[int]], None] = self.dfs
        elif method == "bfs":
            traverse = self.bfs
        else:
            raise ValueError(f"Unknown method: {method}")

        for node in self.adjacency_map:
            if node not in visited:
                component = []
                traverse(node, visited, component)
                components.append(component)

        return components

    def find_center(self) -> List[int]:
        if not self.adjacency_map:
            return []

        degree = {
            node: len(neighbors) for node, neighbors in self.adjacency_map.items()
        }
        leaves = deque(node for node, deg in degree.items() if deg == 1)

        remaining_nodes = len(self.adjacency_map)

        while remaining_nodes > 2:
            leaves_count = len(leaves)
            remaining_nodes -= leaves_count

            for _ in range(leaves_count):
                leaf = leaves.popleft()
                for neighbor in self.adjacency_map[leaf]:
                    degree[neighbor] -= 1
                    if degree[neighbor] == 1:
                        leaves.append(neighbor)

        return list(leaves)

    def find_center_of_star_graph(self) -> int:
        max_degree = -1
        center_node = -1
        for node, neighbors in self.adjacency_map.items():
            if len(neighbors) > max_degree:
                max_degree = len(neighbors)
                center_node = node
        return center_node

    def path_exists_with_bfs(self, source: int, destination: int) -> bool:
        if source == destination:
            return True

        queue = deque([source])
        visited = set()

        while queue:
            node = queue.popleft()
            if node == destination:
                return True
            if node not in visited:
                visited.add(node)
                for neighbor in self.adjacency_map[node]:
                    if neighbor not in visited:
                        queue.append(neighbor)

        return False


def to_adjacency_map(
    graph: List[List[int]], number_of_cities: int
) -> Dict[int, List[int]]:
    adjacency_map: Dict[int, List[int]] = {
        i: [] for i in range(1, number_of_cities + 1)
    }
    for start, end in graph:
        adjacency_map[start].append(end)
        adjacency_map[end].append(start)
    return adjacency_map


def deep_first_search(
    node: int, adjacency_map: Dict[int, List[int]], visited: set
) -> List[int]:
    stack = [node]
    component = []
    while stack:
        current = stack.pop()
        if current not in visited:
            visited.add(current)
            component.append(current)
            stack.extend(adjacency_map[current])
    return component

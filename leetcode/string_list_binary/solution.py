import logging
from os.path import abspath, dirname, join
from typing import List

import logconfig

from leetcode.challenges.solver import Solution

logconfig.from_json(abspath(join(dirname(__file__), "../../logging.json")))
logger = logging.getLogger("root")


class SolutionStringArray(Solution):
    @staticmethod
    def solve(string_list: List[str]) -> int:
        left = curr = ans = 0
        for right in range(len(string_list)):
            if string_list[right] == "0":
                curr += 1
            while curr > 1:
                if string_list[left] == "0":
                    curr -= 1
                left += 1
            ans = max(ans, right - left + 1)

        return ans

import unittest

from leetcode.string_list_binary.solution import SolutionStringArray


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {"data": "1101100111", "expected": 5, "name": "one"},
            ]
        }
        # for case in filter(lambda x: x.get("name") == "one", test_cases.get("cases")):
        for case in test_cases.get("cases"):
            result = SolutionStringArray.solve(case.get("data"))
            self.assertEqual(
                case.get("expected"), result, f"Failed assertion on {case.get('name')}"
            )

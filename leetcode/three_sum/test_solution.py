import unittest

from leetcode.three_sum.solution import Solution


class TestSolution(unittest.TestCase):
    def test_group(self):
        test_cases = {
            1: {"nums": [-1, 0, 1, 2, -1, -4], "expected": [[-1, -1, 2], [-1, 0, 1]]}
        }
        solution = Solution()
        for _, data in test_cases.items():
            result = solution.three_sum(data.get("nums"))
            self.assertEqual(data.get("expected"), result)

    def test_individual(self):
        solution = Solution()
        result = solution.three_sum([-1, 0, 1, 2, -1, -4])
        expected = [[-1, -1, 2], [-1, 0, 1]]
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

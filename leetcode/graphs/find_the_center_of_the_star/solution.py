from typing import List

from utils.graphs.helper import AdjacencyMapHelper


def find_center(edges: List[List[int]]) -> int:
    helper = AdjacencyMapHelper()
    helper.build_from_edges(edges)
    helper.find_connected_components()
    return 0

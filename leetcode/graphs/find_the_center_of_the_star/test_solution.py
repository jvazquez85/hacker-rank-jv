import unittest
from typing import Dict, List

from utils.graphs.helper import AdjacencyMapHelper


class FindCenterOfStarTest(unittest.TestCase):
    def test_create_adjacency_map(self):
        edges = [[1, 2], [2, 3], [4, 2]]
        helper = AdjacencyMapHelper()
        expected: Dict[int, List[int]] = {1: [2], 2: [1, 3, 4], 3: [2], 4: [2]}
        helper.build_from_edges(edges=edges, undirected=True)
        result = helper.get_adjacency_map()
        self.assertEqual(expected, result, "The obtained maps are not equal")

    def test_find_center_of_star_graph(self):
        edges = [[1, 2], [2, 3], [4, 2]]
        helper = AdjacencyMapHelper()
        helper.build_from_edges(edges=edges, undirected=True)
        center = helper.find_center_of_star_graph()
        self.assertEqual(2, center)


if __name__ == "__main__":
    unittest.main()

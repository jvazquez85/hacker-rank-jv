from typing import List


def town_judge(total_people: int, trust: List[List[int]]) -> int:
    if total_people == 1:
        return 1
    trust_counts = [0] * (total_people + 1)

    for a, b in trust:
        trust_counts[a] -= 1
        trust_counts[b] += 1

    for person in range(1, total_people + 1):
        if trust_counts[person] == total_people - 1:
            return person

    return -1

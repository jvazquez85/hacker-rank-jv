import unittest

from leetcode.graphs.town_judge.solution import town_judge


class TestTownJudge(unittest.TestCase):
    def test_case_number_one(self):
        expected = 2
        total_people = 2
        trust = [[1, 2]]
        result = town_judge(trust=trust, total_people=total_people)
        self.assertEqual(expected, result, "Did not find the proper answer")


if __name__ == "__main__":
    unittest.main()

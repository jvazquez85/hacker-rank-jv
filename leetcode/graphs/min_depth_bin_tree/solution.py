from typing import Optional

from utils.trees.utils import TreeNode


def minimum_depth_of_binary_tree(root: Optional[TreeNode]) -> int:
    if not root:
        return 0

    if not root.left:
        return minimum_depth_of_binary_tree(root.right) + 1
    if not root.right:
        return minimum_depth_of_binary_tree(root.left) + 1

    left = minimum_depth_of_binary_tree(root.left)
    right = minimum_depth_of_binary_tree(root.right)
    return min(left, right) + 1

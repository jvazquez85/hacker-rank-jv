import unittest

from leetcode.graphs.min_depth_bin_tree.solution import minimum_depth_of_binary_tree
from utils.trees.utils import create_binary_tree_from_list


class TestMinimumDepthOfBinaryTree(unittest.TestCase):
    def test_case_one(self):
        values = [3, 9, 20, None, None, 15, 7]
        tree = create_binary_tree_from_list(values)
        expected = 2
        self.assertEqual(expected, minimum_depth_of_binary_tree(tree))


if __name__ == "__main__":
    unittest.main()

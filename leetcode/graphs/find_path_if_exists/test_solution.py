import unittest

from leetcode.graphs.find_path_if_exists.solution import valid_path


class TestFindPathIfExists(unittest.TestCase):
    def test_something(self):
        expected = True
        n = 3
        edges = [[0, 1], [1, 2], [2, 0]]
        source = 0
        destination = 2
        self.assertEqual(
            valid_path(n=n, edges=edges, source=source, destination=destination),
            expected,
            "The problem result did not work",
        )


if __name__ == "__main__":
    unittest.main()

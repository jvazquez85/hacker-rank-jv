from typing import List

from utils.graphs.helper import AdjacencyMapHelper


def valid_path(n: int, edges: List[List[int]], source: int, destination: int) -> bool:
    helper = AdjacencyMapHelper(number_of_elements=n)
    helper.build_from_edges(edges=edges, undirected=True)
    return helper.path_exists_with_bfs(source=source, destination=destination)

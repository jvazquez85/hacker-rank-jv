from collections import Counter


class Solution:
    @staticmethod
    def max_number_of_balloons(text: str) -> int:
        char_frequency = Counter(text)

        if len(text) < len("balloon"):
            return 0

        if not all(
            char in char_frequency and char_frequency[char] > 0 for char in "balloon"
        ):
            return 0

        target_frequency = Counter("balloon")

        occurrences = min(
            char_frequency[char] // target_frequency[char] for char in target_frequency
        )

        return occurrences

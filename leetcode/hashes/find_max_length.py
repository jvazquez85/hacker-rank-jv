from typing import List


def find_max_length(nums: List[int]) -> int:
    count = 0
    max_length = 0
    prefix_sum = {0: -1}

    for i in range(len(nums)):
        if nums[i] == 0:
            count -= 1
        else:
            count += 1

        if count in prefix_sum:
            max_length = max(max_length, i - prefix_sum[count])
        else:
            prefix_sum[count] = i

    return max_length

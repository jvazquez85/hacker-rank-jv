from typing import List


class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        nums.sort()
        missing = None
        top = len(nums)
        left = 0
        right = 1

        while left < top and right < top:
            if nums[left] + 1 != nums[right]:
                missing = nums[left] + 1
                break
            left += 1
            right += 1

        if missing is None:
            if 0 not in nums:
                missing = 0
            elif nums[0] != top:
                missing = top
            else:
                missing = top - 1

        return missing

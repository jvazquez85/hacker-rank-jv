import unittest

from leetcode.hashes.missing_number.app import Solution


class SolutionTestCase(unittest.TestCase):
    def test_missing_numbers_example_zero(self):
        solution = Solution()
        example = [3, 0, 1]
        expected = 2
        response = solution.missingNumber(example)
        self.assertEqual(expected, response, f"Expected {expected} but got {response}")

    def test_missing_numbers_example_one(self):
        solution = Solution()
        example = [0, 1]
        expected = 2
        response = solution.missingNumber(example)
        self.assertEqual(expected, response, f"Expected {expected} but got {response}")

    def test_missing_numbers_example_two(self):
        solution = Solution()
        example = [1]
        expected = 0
        response = solution.missingNumber(example)
        self.assertEqual(expected, response, f"Expected {expected} but got {response}")


if __name__ == "__main__":
    unittest.main()

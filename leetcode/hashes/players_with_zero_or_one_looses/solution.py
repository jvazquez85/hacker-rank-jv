from collections import defaultdict
from typing import List

from leetcode.challenges.solver import Solution


class Players(Solution):
    @staticmethod
    def solve(matches: List[List[int]]) -> List[List[int]]:
        players = defaultdict(list)

        for winner, looser in matches:
            if winner not in players.keys():
                players[winner] = [1]
            else:
                players[winner].append(1)

            if looser not in players.keys():
                players[looser] = [0]
            else:
                players[looser].append(0)
        loosers = []
        winners = []
        for player in players:
            if players[player].count(0) == 0:
                winners.append(player)
            if players[player].count(0) == 1:
                loosers.append(player)
        loosers.sort()
        winners.sort()
        return [winners, loosers]

    @staticmethod
    def failed_solution_one(matches: List[List[int]]) -> List[List[int]]:
        # Times out on a huge list
        results = {"winners": [], "loosers": []}

        for game in matches:
            winner, looser = game
            results["winners"].append(winner)
            results["loosers"].append(looser)

        real_winners = list(
            set(filter(lambda x: x not in results["loosers"], results["winners"]))
        )
        real_winners.sort()
        real_loosers = list(
            filter(lambda x: results["loosers"].count(x) == 1, results["loosers"])
        )
        real_loosers.sort()
        return [real_winners, real_loosers]


if __name__ == "__main__":
    test_case = [
        [1, 3],
        [2, 3],
        [3, 6],
        [5, 6],
        [5, 7],
        [4, 5],
        [4, 8],
        [4, 9],
        [10, 4],
        [10, 9],
    ]
    expected_solution = [[1, 2, 10], [4, 5, 7, 8]]
    result = Players.solve(matches=test_case)
    assert expected_solution == result, f"Expected {expected_solution}. Got\n {result}"

from collections import defaultdict
from typing import List

from leetcode.challenges.solver import Solution


class LargestUniqueNumber(Solution):
    @staticmethod
    def solve(nums: List[int]) -> int:
        hashmap = defaultdict(int)
        max_number = -1

        for number in nums:
            hashmap[number] += 1

        for number, times in hashmap.items():
            if times == 1:
                max_number = max(max_number, number)

        return max_number


if __name__ == "__main__":
    test_case = [5, 7, 3, 9, 4, 9, 8, 3, 1]
    expected_solution = 8
    result = LargestUniqueNumber.solve(nums=test_case)
    assert expected_solution == result, f"Expected {expected_solution}. Got\n {result}"

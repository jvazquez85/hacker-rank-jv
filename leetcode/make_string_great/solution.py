# i did this one in the editor
class Solution:
    def makeGood(self, s: str) -> str:
        if len(s) == 1:
            return s

        partial = []
        for char in s:
            if partial and partial[-1].lower() == char.lower() and partial[-1] != char:
                partial.pop()
            else:
                partial.append(char)

        return "".join(partial)

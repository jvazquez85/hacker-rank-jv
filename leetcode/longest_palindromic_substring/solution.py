class Solution:
    def longest_palindrome(self, s: str) -> str:
        def is_palindrome(substr: str) -> bool:
            return substr == substr[::-1]

        max_length = 0
        longest_palindrome = ""
        word_length = len(s)
        for i in range(word_length):
            for j in range(i + 1, word_length + 1):
                substr = s[i:j]
                if is_palindrome(substr) and len(substr) > max_length:
                    max_length = len(substr)
                    longest_palindrome = substr
        return longest_palindrome

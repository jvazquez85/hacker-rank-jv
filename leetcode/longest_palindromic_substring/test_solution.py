import unittest

from leetcode.longest_palindromic_substring.solution import Solution


class TestSolution(unittest.TestCase):
    def test_something(self):
        test_case = "babad"
        expected = "bab"
        solution = Solution()
        solution.longest_palindrome(test_case)
        self.assertEqual(solution.longest_palindrome(test_case), expected)


if __name__ == "__main__":
    unittest.main()

import unittest

from leetcode.contains_duplicate.solution import ContainsDuplicate


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {"data": [1, 2, 3, 1], "expected": True, "name": "one"},
                {"data": [1, 2, 3, 4], "expected": False, "name": "two"},
                {
                    "data": [1, 1, 1, 3, 3, 4, 3, 2, 4, 2],
                    "expected": True,
                    "name": "three",
                },
            ]
        }
        for case in test_cases.get("cases"):
            result = ContainsDuplicate.solve(case.get("data"))
            if case.get("expected"):
                self.assertTrue(result, f"Failed assertion on {case.get('name')}")
            else:
                self.assertFalse(result, f"Failed assertion on {case.get('name')}")

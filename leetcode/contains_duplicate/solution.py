from typing import List


class ContainsDuplicate:
    @staticmethod
    def solve(nums: List[int]) -> bool:
        return len(nums) > len(set(nums))


if __name__ == "__main__":
    case_one = [1, 2, 3, 1]
    ContainsDuplicate.solve(nums=case_one)

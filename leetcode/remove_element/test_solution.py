import unittest

from leetcode.remove_element.solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example_method(self):
        expected_result = 2
        nums = [3, 2, 2, 3]
        val = 3
        self.assertEqual(expected_result, self.solution.remove_element(nums, val))
        print(nums)
        for i in range(len(nums)):
            self.assertEqual(nums[i] != val, True)


if __name__ == "__main__":
    unittest.main()

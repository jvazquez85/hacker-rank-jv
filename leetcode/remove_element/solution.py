from collections import Counter
from typing import List


class Solution:
    def remove_element(self, nums: List[int], val: int) -> int:
        frequency_map = Counter(nums)
        not_equal_to_val = sum(j for i, j in frequency_map.items() if i != val)
        start = 0
        while start < len(nums):
            if nums[start] == val:
                nums.pop(start)
            else:
                start += 1
        return not_equal_to_val

    # A better solution would be to just iterate and pop, and
    # return the len(nums) after cleanup

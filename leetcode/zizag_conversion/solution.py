from typing import List


class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s
        rows = [""] * numRows
        current_row = 0
        going_down = False
        for letter in s:
            rows[current_row] += letter
            if current_row == 0 or current_row == numRows - 1:
                going_down = not going_down
            current_row += 1 if going_down else -1

        return "".join(rows)

    def convert_better(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s
        rows: List[List[str]] = [[] for _ in range(numRows)]
        current_row = 0
        going_down = False

        for char in s:
            rows[current_row].append(char)
            if current_row == 0 or current_row == numRows - 1:
                going_down = not going_down
            current_row += 1 if going_down else -1

        row_strings: List[str] = ["".join(row) for row in rows]

        return "".join(row_strings)

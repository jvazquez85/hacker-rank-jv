import unittest

from leetcode.zizag_conversion.solution import Solution


class TestSolution(unittest.TestCase):
    def test_simple(self):
        solution = Solution()
        expected = "PAHNAPLSIIGYIR"
        num_rows = 3
        s = "PAYPALISHIRING"
        result = solution.convert(s=s, numRows=num_rows)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False

        if 0 <= x < 10:
            return True

        if x % 10 == 0 and x != 0:
            return False

        reversed = 0
        original = x

        while x > 0:
            reversed = reversed * 10 + x % 10
            x //= 10
        return original == reversed

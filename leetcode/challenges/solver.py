from abc import ABC


class Solution(ABC):
    @staticmethod
    def solve():
        raise NotImplementedError

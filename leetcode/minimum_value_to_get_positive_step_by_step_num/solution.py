from typing import List


def min_start_value(nums: List[int]) -> int:
    prefix_sum = 0
    min_value = 0

    for num in nums:
        prefix_sum += num
        min_value = min(min_value, prefix_sum)

    start_value = 1 - min_value
    return start_value

import unittest

from leetcode.minimum_value_to_get_positive_step_by_step_num.solution import (
    min_start_value,
)


class TestMinStartValue(unittest.TestCase):
    def test_something(self):
        nums = [-3, 2, -3, 4, 2]
        result = min_start_value(nums)
        expected = 5
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

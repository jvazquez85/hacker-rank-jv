import unittest

from leetcode.string_reverse.solution import SolutionString


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {
                    "data": ["h", "e", "l", "l", "o"],
                    "expected": ["o", "l", "l", "e", "h"],
                    "name": "one",
                },
                {
                    "data": ["H", "a", "n", "n", "a", "h"],
                    "expected": ["h", "a", "n", "n", "a", "H"],
                    "name": "two",
                },
            ]
        }
        # for case in filter(lambda x: x.get("name") == "one", test_cases.get("cases")):
        for case in test_cases.get("cases"):
            data = case.get("data")
            SolutionString.solve(data)
            self.assertEqual(
                case.get("expected"), data, f"Failed assertion on {case.get('name')}"
            )

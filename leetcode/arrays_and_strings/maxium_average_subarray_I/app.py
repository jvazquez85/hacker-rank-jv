from typing import List


class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        curr_sum = sum(nums[:k])
        max_avg = curr_sum / k
        left = 0

        for right in range(k, len(nums)):
            curr_sum += nums[right] - nums[left]
            max_avg = max(max_avg, curr_sum / k)
            left += 1

        return max_avg

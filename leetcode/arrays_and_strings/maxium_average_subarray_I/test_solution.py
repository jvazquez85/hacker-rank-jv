import unittest

from leetcode.arrays_and_strings.maxium_average_subarray_I.app import Solution


class TestMaximumAverageSubarrayOne(unittest.TestCase):
    def test_sample_one(self):
        expected = 12.75000
        nums = [1, 12, -5, -6, 50, 3]
        k = 4
        solution = Solution()
        response = solution.findMaxAverage(nums=nums, k=k)
        print(response)
        self.assertEqual(
            response, expected, f"Expected {expected} but obtained {response}"
        )


if __name__ == "__main__":
    unittest.main()

import unittest

from leetcode.arrays_and_strings.max_consecutive_ones_III.app import Solution


class TestLongestOnesCase(unittest.TestCase):
    def test_sample_explanation(self):
        nums = [1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0]
        k = 2
        expected = 6
        solution = Solution()
        result = solution.longest_ones(nums, k)
        self.assertEqual(
            expected, result, f"Expected {expected}, but obtained {result}"
        )


if __name__ == "__main__":
    unittest.main()

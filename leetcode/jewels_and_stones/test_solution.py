import unittest

from leetcode.jewels_and_stones.solution import numJewelsInStones


class TestSolution(unittest.TestCase):
    def test_case_one(self):
        jewels = "aA"
        stones = "aAAbbbb"
        expected = 3
        result = numJewelsInStones(jewels, stones)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

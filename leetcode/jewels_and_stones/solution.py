from collections import Counter


def numJewelsInStones(jewels: str, stones: str) -> int:
    stone_frequency = Counter(stones)
    jewel_frequency = Counter(jewels)
    stone_keys = set(stone_frequency.keys())
    jewel_keys = set(jewel_frequency.keys())
    matches = stone_keys & jewel_keys
    total = 0
    for match in matches:
        total += stone_frequency[match]
    return total

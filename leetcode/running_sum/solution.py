from typing import List


class Solution:
    def running_sum(self, nums: List[int]) -> List[int]:
        prefix = [nums[0]]
        start, end = 1, len(nums)
        while start < end:
            print(f"{prefix[-1]} + {nums[start]} = {prefix[-1] + nums[start]}")
            prefix.append(prefix[-1] + nums[start])
            start += 1
        return prefix

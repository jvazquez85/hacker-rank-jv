import unittest

from leetcode.running_sum.solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example_method(self):
        nums = [3, 1, 2, 10, 1]
        expected = [3, 4, 6, 16, 17]
        self.assertEqual(self.solution.running_sum(nums), expected)


if __name__ == "__main__":
    unittest.main()

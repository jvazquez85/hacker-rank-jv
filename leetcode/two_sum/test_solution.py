import unittest

from leetcode.two_sum.solution import TwoSum


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {
                    "data": {"nums": [2, 7, 11, 15], "target": 9},
                    "expected": [0, 1],
                    "name": "one",
                },
                {
                    "data": {"nums": [3, 2, 4], "target": 6},
                    "expected": [1, 2],
                    "name": "two",
                },
                {
                    "data": {"nums": [3, 2, 3], "target": 6},
                    "expected": [0, 2],
                    "name": "two",
                },
            ]
        }
        # for case in filter(lambda x: x.get("name") == "one", test_cases.get("cases")):
        for case in test_cases.get("cases"):
            result = TwoSum.solve(**case.get("data"))
            self.assertEqual(
                case.get("expected"), result, f"Failed assertion on {case.get('name')}"
            )

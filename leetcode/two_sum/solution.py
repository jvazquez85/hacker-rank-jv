import logging
from os.path import abspath, dirname, join
from typing import List

import logconfig

from leetcode.challenges.solver import Solution

logconfig.from_json(abspath(join(dirname(__file__), "../../logging.json")))
logger = logging.getLogger("root")


class TwoSum(Solution):
    @staticmethod
    def solve(nums: List[int], target: int) -> List[int]:
        solution = []
        for index, number in enumerate(nums):
            shifted_index = index + 1
            while shifted_index < len(nums):
                if number + nums[shifted_index] == target:
                    solution.append(index)
                    solution.append(shifted_index)
                    break
                shifted_index += 1
            if len(solution):
                break
        return solution

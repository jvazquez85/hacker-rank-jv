package my_atoi

import "math"

func MyAtoi(s string) int {
	i := 0
	n := len(s)
	sign := 1
	result := 0

	for i < n && s[i] == ' ' {
		i++
	}

	if i < n && (s[i] == '-' || s[i] == '+') {
		if s[i] == '-' {
			sign = -1
		}
		i++
	}

	for i < n && s[i] >= '0' && s[i] <= '9' {
		digit := int(s[i] - '0')
		if result > (math.MaxInt32-digit)/10 {
			if sign == 1 {
				return math.MaxInt32
			}
			return math.MinInt32
		}
		result = result*10 + digit
		i++
	}

	return result * sign
}

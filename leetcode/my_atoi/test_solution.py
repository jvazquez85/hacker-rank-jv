import unittest

from leetcode.my_atoi.solution import Solution


class TestSolution(unittest.TestCase):
    def test_case_simple(self):
        s = "42"
        expected = 42
        solution = Solution()
        result = solution.myAtoi(s)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

from leetcode.challenges.solver import Solution


class FirstLetter(Solution):
    @staticmethod
    def solve(s: str) -> str:
        seen = set()
        for letter in s:
            if letter in seen:
                return letter
            seen.add(letter)
        return " "


if __name__ == "__main__":
    test_case = "abcdeda"
    expected_solution = "d"
    assert expected_solution == FirstLetter.solve(s=test_case)

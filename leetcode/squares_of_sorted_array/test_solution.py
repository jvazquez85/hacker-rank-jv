import unittest

from leetcode.squares_of_sorted_array.solution import SolutionSquare


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {
                    "data": [-4, -1, 0, 3, 10],
                    "expected": [0, 1, 9, 16, 100],
                    "name": "one",
                },
                {
                    "data": [-7, -3, 2, 3, 11],
                    "expected": [4, 9, 9, 49, 121],
                    "name": "two",
                },
            ]
        }
        # for case in filter(lambda x: x.get("name") == "one", test_cases.get("cases")):
        for case in test_cases.get("cases"):
            result = SolutionSquare.solve(case.get("data"))
            self.assertEqual(
                case.get("expected"), result, f"Failed assertion on {case.get('name')}"
            )

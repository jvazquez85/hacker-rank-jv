import logging
from os.path import abspath, dirname, join
from typing import List

import logconfig

from leetcode.challenges.solver import Solution

logconfig.from_json(abspath(join(dirname(__file__), "../../logging.json")))
logger = logging.getLogger("root")


class SolutionSquare(Solution):
    @staticmethod
    def solve(nums: List[int]) -> List[int]:
        return sorted(map(lambda x: x**2, nums))

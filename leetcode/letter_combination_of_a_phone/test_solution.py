import unittest

from leetcode.letter_combination_of_a_phone.solution import Solution


class MyTestCase(unittest.TestCase):
    def test_combined(self):
        test_cases = {
            1: {
                "digits": "23",
                "expected": ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"],
            },
            2: {"digits": "", "expected": []},
            3: {"digits": "2", "expected": ["a", "b", "c"]},
        }
        solution = Solution()
        for _, test_case in test_cases.items():
            result = solution.letterCombinations(test_case["digits"])
            self.assertEqual(test_case["expected"], result)

    def test_single(self):
        solution = Solution()
        result = solution.letterCombinations("23")
        self.assertEqual(["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"], result)


if __name__ == "__main__":
    unittest.main()

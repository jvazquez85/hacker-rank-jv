from typing import List


class Solution:
    def reverse_string(self, s: List[str]) -> None:
        """Reverse the string in place"""
        left, right = 0, len(s) - 1
        while left < right:
            s[left], s[right] = s[right], s[left]
            left += 1
            right -= 1

import unittest

from leetcode.easy_challenges.strings.reverse_string.solution import Solution


class TestReverseString(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example(self):
        s = ["h", "e", "l", "l", "o"]
        expected = ["o", "l", "l", "e", "h"]
        self.solution.reverse_string(s)
        self.assertEqual(expected, s)

    def test_example_two(self):
        s = ["H", "a", "n", "n", "a", "h"]
        expected = ["h", "a", "n", "n", "a", "H"]
        self.solution.reverse_string(s)
        self.assertEqual(expected, s)


if __name__ == "__main__":
    unittest.main()

import unittest

from leetcode.easy_challenges.strings.first_unique_character.solution import Solution


class TestFirstUniqueChar(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example_one(self):
        s = "leetcode"
        expected_result = 0
        result = self.solution.first_unique_char(s)
        self.assertEqual(expected_result, result)

    def test_example_two(self):
        s = "loveleetcode"
        expected_result = 2
        result = self.solution.first_unique_char(s)
        self.assertEqual(expected_result, result)

    def test_example_three(self):
        s = "aabb"
        expected_result = -1
        result = self.solution.first_unique_char(s)
        self.assertEqual(expected_result, result)


if __name__ == "__main__":
    unittest.main()

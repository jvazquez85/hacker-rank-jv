from collections import defaultdict


class Solution:
    def first_unique_char(self, word: str) -> int:
        if len(word) == 1:
            return 0

        character_map = defaultdict(int)
        for character in word:
            if character_map.get(character):
                character_map[character] += 1
            else:
                character_map[character] = 1

        if any(count == 1 for count in character_map.values()) is False:
            return -1
        else:
            for char, count in character_map.items():
                if count == 1:
                    return word.index(char)

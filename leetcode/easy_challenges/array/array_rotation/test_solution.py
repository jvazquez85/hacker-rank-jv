import unittest

from leetcode.easy_challenges.array.array_rotation.solution import Solution


class TestSolution(unittest.TestCase):
    def test_something(self):
        nums = [1, 2, 3, 4, 5, 6, 7]
        k = 3
        expected = [5, 6, 7, 1, 2, 3, 4]
        solution = Solution()
        solution.rotate(nums, k)
        self.assertEqual(expected, nums)


if __name__ == "__main__":
    unittest.main()

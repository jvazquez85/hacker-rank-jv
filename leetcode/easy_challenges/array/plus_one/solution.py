from typing import List


class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        carry = 1  # Start with the carry to account for the +1
        index = len(digits) - 1  # Start from the last digit

        while index >= 0:
            digits[index] += carry

            if digits[index] == 10:
                digits[index] = 0
                carry = 1
            else:
                carry = 0
                break

            index -= 1

        # If there's still a carry left after processing all digits
        if carry == 1:
            digits.insert(0, 1)

        return digits

import unittest

from leetcode.easy_challenges.array.plus_one.solution import Solution


class TestSolution(unittest.TestCase):
    def test_simple(self):
        expected = [1, 0]
        test_case = [9]
        solution = Solution()
        result = solution.plusOne(test_case)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

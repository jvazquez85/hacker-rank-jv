from functools import reduce
from typing import List


class Solution:
    def has_no_duplicates(self, nums: List[int]):
        bitmask = 0

        for num in nums:
            mask = 1 << num
            if bitmask & mask:
                return False
            bitmask |= mask

        return True

    def convert_row_to_integers(self, row: List[str]) -> List[int]:
        return [int(n) for n in row if n.isdigit()]

    def valid_sudoku(self, board: List[List[str]]) -> bool:
        start, end = 0, 8
        valid_rows = reduce(
            lambda acc, row: acc
            and self.has_no_duplicates(self.convert_row_to_integers(row)),
            board,
            True,
        )
        if not valid_rows:
            return False

        cols_valid = reduce(
            lambda acc, col_idx: acc
            and self.has_no_duplicates(
                self.convert_row_to_integers(
                    [board[row_idx][col_idx] for row_idx in range(start, end + 1)]
                )
            ),
            range(start, end + 1),
            True,
        )

        if not cols_valid:
            return False

        for row in range(0, end + 1, 3):
            for col in range(0, end + 1, 3):
                subgrid = [
                    board[r][c]
                    for r in range(row, row + 3)
                    for c in range(col, col + 3)
                ]
                if not self.has_no_duplicates(self.convert_row_to_integers(subgrid)):
                    return False

        return True


# This seems to be faster, what I like here is the early exit though but the
# grid_index = (i // 3) * 3 + (j // 3) , i would never came up with that in a challenge
# def valid_sudoku(self, board: List[List[str]]) -> bool:
#     rows = [set() for _ in range(9)]
#     cols = [set() for _ in range(9)]
#     grids = [set() for _ in range(9)]
#
#     for i in range(9):
#         for j in range(9):
#             num = board[i][j]
#             if num.isdigit():
#                 grid_index = (i // 3) * 3 + (j // 3)
"""
- i // subgrid_rows: This determines the row of subgrids the current cell
    (i, j) belongs to.
  It divides the cell's row index i by the number of rows in each subgrid,
  subgrid_rows.
- j // subgrid_cols: This determines the column of subgrids the current
    cell(i, j) belongs to.
  It divides the cell's column index j by the number of columns in each subgrid,
  subgrid_cols.
- num_subgrid_cols: This represents the total number of subgrids along the
    width of the grid, calculated as grid_cols // subgrid_cols,
    where grid_cols is the total number of columns in the grid

# For a standard 9x9 Sudoku grid with 3x3 subgrids:

    subgrid_rows = 3
    subgrid_cols = 3
    num_subgrid_cols = 9 // 3 = 3

## Subgrid Index Calculation:

grid_index = (i // 3) * 3 + (j // 3)

This formula calculates the subgrid index for each cell (i, j) in the 9x9 grid.
The 9x9 grid is divided into nine 3x3 subgrids, with indices from 0 to 8.

## For example:

    Cell (2, 2) falls in subgrid 0 (top-left corner).
    Cell (4, 4) falls in subgrid 4 (center).
    Cell (8, 8) falls in subgrid 8 (bottom-right corner).

This calculation helps in segmenting the grid into subgrids,
allowing for efficient and independent validation of each subgrid's contents.
"""
#                 if num in rows[i] or num in cols[j] or num in grids[grid_index]:
#                     return False
#
#                 rows[i].add(num)
#                 cols[j].add(num)
#                 grids[grid_index].add(num)
#
#     return True

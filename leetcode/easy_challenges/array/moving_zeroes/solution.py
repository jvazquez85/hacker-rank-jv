from typing import List


class Solution:
    def move_zeroes(self, nums: List[int]) -> None:
        start, end = 0, len(nums) - 1
        while start <= end:
            if nums[start] == 0:
                zero = nums.pop(start)
                nums.append(zero)
                end -= 1
            else:
                start += 1

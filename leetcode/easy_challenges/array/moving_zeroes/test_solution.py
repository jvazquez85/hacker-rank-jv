import unittest

from leetcode.easy_challenges.array.moving_zeroes.solution import Solution


class TestMovingZeroes(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_simple_case(self):
        nums = [0, 1, 0, 3, 12]
        expected_result = [1, 3, 12, 0, 0]
        self.solution.move_zeroes(nums)
        self.assertEqual(nums, expected_result)

    def test_failing(self):
        nums = [0, 0, 1]
        expected_result = [1, 0, 0]
        self.solution.move_zeroes(nums)
        self.assertEqual(nums, expected_result)


if __name__ == "__main__":
    unittest.main()

import unittest

from leetcode.easy_challenges.array.two_sum.solution import Solution


class TestTwoSum(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example_method(self):
        nums = [3, 2, 4]
        target = 6
        expected_result = [1, 2]
        result = self.solution.two_sum(nums, target)
        self.assertEqual(expected_result, result)

    def test_combined(self):
        test_cases = {
            "cases": [
                {
                    "data": {"nums": [2, 7, 11, 15], "target": 9},
                    "expected": [0, 1],
                    "name": "one",
                },
                {
                    "data": {"nums": [3, 2, 4], "target": 6},
                    "expected": [1, 2],
                    "name": "two",
                },
                {
                    "data": {"nums": [3, 2, 3], "target": 6},
                    "expected": [0, 2],
                    "name": "three",
                },
                {
                    "data": {"nums": [1, 3, 4, 2], "target": 6},
                    "expected": [2, 3],
                    "name": "four",
                },
            ]
        }
        for case in test_cases.get("cases"):
            result = self.solution.two_sum(**case.get("data"))
            self.assertEqual(
                case.get("expected"),
                result,
                f"Failed assertion on {case.get('name')}. Expected"
                f"{case.get('expected')}, got {result}",
            )


if __name__ == "__main__":
    unittest.main()

from typing import List


class Solution:
    def two_sum(self, nums: List[int], target: int) -> List[int]:
        for left in range(len(nums) - 1):
            right = len(nums) - 1
            while left < right:
                result = nums[left] + nums[right]
                if result == target:
                    return [left, right]
                else:
                    right -= 1

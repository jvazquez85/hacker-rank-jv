import unittest

from leetcode.easy_challenges.array.intersection_of_two_arrays.solution import (
    intersection,
)


class TestIntersection(unittest.TestCase):
    def test_simple(self):
        nums1 = [1, 2, 2, 1]
        nums2 = [2]
        expected = [2]
        result = intersection(nums1, nums2)
        self.assertEqual(expected, result)

    def test_another(self):
        nums1 = [3, 1, 2]
        nums2 = [1, 1]
        result = intersection(nums1, nums2)
        expected = [1]
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

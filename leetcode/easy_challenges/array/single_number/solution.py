from typing import List

if __name__ == "__main__":
    nums: List[int] = [2, 3, 5, 4, 5, 3, 4]
    non_unique = 0
    for num in nums:
        non_unique ^= num

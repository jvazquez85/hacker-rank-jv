from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        profit = 0
        for today in range(len(prices) - 1):
            tomorrow = today + 1
            if prices[today] < prices[tomorrow]:
                profit += prices[tomorrow] - prices[today]

        return profit

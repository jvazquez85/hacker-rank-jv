import unittest

from leetcode.easy_challenges.array.max_profit.solution import Solution


class TestSolution(unittest.TestCase):
    def test_simple(self):
        sample = [7, 1, 5, 3, 6, 4]
        expected = 7
        solution = Solution()
        result = solution.maxProfit(sample)
        self.assertEqual(expected, result)

    def test_combined(self):
        test_cases = {
            1: {"prices": [7, 6, 4, 3, 1], "expected": 0},
        }
        solution = Solution()
        for _, test_case in test_cases.items():
            result = solution.maxProfit(test_case.get("prices"))
            self.assertEqual(test_case.get("expected"), result)


if __name__ == "__main__":
    unittest.main()

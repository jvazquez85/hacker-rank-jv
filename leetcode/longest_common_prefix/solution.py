from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        m = len(min(strs, key=len))
        low, high = 0, m

        while low <= high:
            mid = (low + high) // 2
            if all(s[:mid] == strs[0][:mid] for s in strs):
                low = mid + 1
            else:
                high = mid - 1
        return strs[0][: (low + high) // 2]

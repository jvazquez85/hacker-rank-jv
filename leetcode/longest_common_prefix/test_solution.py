import unittest

from leetcode.longest_common_prefix.solution import Solution


class TestSolution(unittest.TestCase):
    def test_combined(self):
        test_cases = {
            1: {"words": ["flower", "flow", "flight"], "expected": "fl"},
            2: {"words": ["dog", "racecar", "car"], "expected": ""},
            3: {"words": ["ab", "a"], "expected": "a"},
            4: {"words": ["cir", "car"], "expected": "c"},
        }
        solution = Solution()
        for _, test_case in enumerate(test_cases.values()):
            response = solution.longestCommonPrefix(test_case.get("words"))
            self.assertEqual(response, test_case.get("expected"))

    def test_complex(self):
        strs = ["flower", "flow", "flight"]
        expected = "fl"
        solution = Solution()
        response = solution.longestCommonPrefix(strs)
        self.assertEqual(expected, response)


if __name__ == "__main__":
    unittest.main()

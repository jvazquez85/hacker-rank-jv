import unittest

from leetcode.three_sum_closest.solution import Solution


class TestSolution(unittest.TestCase):
    def test_group(self):
        test_cases = {1: {"nums": [-1, 2, 1, -4], "target": 1, "expected": 2}}
        solution = Solution()
        for _, data in test_cases.items():
            result = solution.three_sum_closest(data.get("nums"), data.get("target"))
            self.assertEqual(data.get("expected"), result)

    def test_individual(self):
        solution = Solution()
        target = 1
        result = solution.three_sum_closest([0, 0, 0], target)
        expected = 0
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

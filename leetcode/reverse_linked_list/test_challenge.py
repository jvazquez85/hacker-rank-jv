import unittest

from leetcode.reverse_linked_list.solution import ListNode, Solution


def lists_equal(l1, l2):
    while l1 and l2:
        if l1.val != l2.val:
            return False
        l1, l2 = l1.next, l2.next
    return l1 is None and l2 is None


class TestReverseLinkedList(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example_one(self):
        head = ListNode(1)
        current = head
        for val in range(2, 6):
            node = ListNode(val)
            current.next = node
            current = current.next

        left, right = 2, 4

        expected = ListNode(1)
        expected.next = ListNode(4)
        expected.next.next = ListNode(3)
        expected.next.next.next = ListNode(2)
        expected.next.next.next.next = ListNode(5)

        self.assertTrue(
            lists_equal(expected, self.solution.reverse_linked_list(head, left, right))
        )

    def test_example_two(self):
        head = ListNode(5)
        left, right = 1, 1
        self.assertEqual(head, self.solution.reverse_linked_list(head, left, right))


if __name__ == "__main__":
    unittest.main()

class Solution:
    def longest_repeating_substring(self, s: str) -> int:
        n = len(s)
        max_len = 0

        for window_size in range(1, n):
            seen = set()
            for i in range(n - window_size + 1):
                substring = s[i : i + window_size]
                if substring in seen:
                    max_len = max(max_len, window_size)
                seen.add(substring)

        return max_len

import unittest

from leetcode.longest_repeating_substring.solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example_method(self):
        s = "abbaba"
        expected_result = 2
        self.assertEqual(expected_result, self.solution.longest_repeating_substring(s))


if __name__ == "__main__":
    unittest.main()

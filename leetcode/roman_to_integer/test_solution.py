import unittest

from leetcode.roman_to_integer.solution import Solution


class TestRomanToInteger(unittest.TestCase):
    def test_solution(self):
        test_number = {
            "III": 3,
            "IV": 4,
            "VIII": 8,
            "IX": 9,
            "XL": 40,
            "XIV": 14,
            "IXL": 41,
            "MCMXCIV": 1994,
            "MCDLIII": 1453,
            "MDCLXVI": 1666,
            "MMMCMXCIX": 3999,
        }
        solver = Solution()
        for s, expected in test_number.items():
            test_result = solver.roman_to_int(s=s)
            self.assertEqual(
                expected,
                test_result,
                f"Test case failed for {s}."
                f"Expected: {expected}, got: {test_result}",
            )

    def test_specific_case(self):
        test_number = {"MMMCMXCIX": 3999}
        solver = Solution()
        for s, expected in test_number.items():
            result = solver.roman_to_int(s=s)
            self.assertEqual(
                expected,
                result,
                f"Test case failed for {s}." f"Expected: {expected}, got: {result}",
            )

    def test_annoying_ones(self):
        test_number = {"XIV": 14, "IXL": 41}
        solver = Solution()
        for s, expected in test_number.items():
            result = solver.roman_to_int(s=s)
            self.assertEqual(
                expected,
                result,
                f"Test case failed for {s}." f"Expected: {expected}, got: {result}",
            )


if __name__ == "__main__":
    unittest.main()

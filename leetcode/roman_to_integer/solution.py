roman_map = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}


def should_subtract(first_roman_number: str, next_roman_number: str) -> bool:
    return roman_map[first_roman_number] > roman_map[next_roman_number]


def position_in_string_index(index: int, str_len: int) -> bool:
    return 0 <= index <= str_len - 1


class Solution:
    def roman_to_int(self, s: str) -> int:
        number = 0
        str_len = len(s)
        visited_indexes = set()
        i = str_len - 1

        while i >= 0:
            current_digit = s[i]
            if i in visited_indexes:
                i -= 1
                continue

            next_position = i - 1
            if position_in_string_index(next_position, str_len):
                next_digit = s[next_position]
                if should_subtract(current_digit, next_digit):
                    if roman_map[s[next_position]] > roman_map[current_digit]:
                        number += (
                            roman_map[next_digit] - roman_map[current_digit]
                        ) * -1
                    else:
                        number += roman_map[current_digit] - roman_map[s[next_position]]
                    visited_indexes.add(next_position)
                else:
                    number += roman_map[current_digit]
            else:
                number += roman_map[current_digit]
            i -= 1
        return number

import unittest

from leetcode.intersection_of_two_arrays.solution import IntersectionOfTwoArrays


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {
                    "data": {"nums1": [1, 2, 2, 1], "nums2": [2, 2]},
                    "expected": [2, 2],
                    "name": "one",
                },
                {
                    "data": {"nums1": [4, 9, 5], "nums2": [9, 4, 9, 8, 4]},
                    "expected": [4, 9],
                    "name": "two",
                },
            ]
        }
        # for case in filter(lambda x: x.get("name") == "one", test_cases.get("cases")):
        for case in test_cases.get("cases"):
            result = IntersectionOfTwoArrays.solve(**case.get("data"))
            self.assertEqual(
                case.get("expected"), result, f"Failed assertion on {case.get('name')}"
            )

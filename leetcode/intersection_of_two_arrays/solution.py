import logging
from os.path import abspath, dirname, join
from typing import List

import logconfig

from leetcode.challenges.solver import Solution

logconfig.from_json(abspath(join(dirname(__file__), "../../logging.json")))
logger = logging.getLogger("root")


class IntersectionOfTwoArrays(Solution):
    @staticmethod
    def solve(nums1: List[int], nums2: List[int]) -> List[int]:
        result = []
        for i in nums1:
            for j in nums2:
                logger.info(f"Checking if {nums1[i]} exists on {nums2}")
                if nums1[i] in nums2:
                    if result.count(nums1[i]) < nums1.count(nums1[i]):
                        logger.info(f"Count of {nums1[i]} ")
                        result.append(nums1[i])

        return result

class StockSpanner:

    def __init__(self):
        self.price_span = []

    def next(self, price: int) -> int:
        span = 1
        while self.price_span and self.price_span[-1][0] <= price:
            _, prev_span = self.price_span.pop()
            span += prev_span
        self.price_span.append((price, span))
        return span

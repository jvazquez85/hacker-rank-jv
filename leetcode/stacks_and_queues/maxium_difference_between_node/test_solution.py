import unittest

from leetcode.stacks_and_queues.maxium_difference_between_node.solution import (
    max_ancestor_diff,
)
from utils.trees.utils import create_binary_tree_from_list


class MaxAncestorTest(unittest.TestCase):
    def test_case_one(self):
        values = [8, 3, 10, 1, 6, None, 14, None, None, 4, 7, 13]
        expected = 7
        tree = create_binary_tree_from_list(values)
        result = max_ancestor_diff(tree)
        self.assertEqual(expected, result, f"Expected: {expected}, Actual: {result}")


if __name__ == "__main__":
    unittest.main()

from typing import Optional

from utils.trees.utils import TreeNode


def max_ancestor_diff(root: Optional[TreeNode]) -> int:
    def node_evaluation(node: Optional[TreeNode], min_val: int, max_val: int) -> int:
        if node is None:
            return max_val - min_val

        # Update min and max for the current path
        min_val = min(min_val, node.val)
        max_val = max(max_val, node.val)

        # Recursively evaluate left and right subtrees
        left = node_evaluation(node.left, min_val, max_val)
        right = node_evaluation(node.right, min_val, max_val)

        # Return the maximum difference found in either subtree
        return max(left, right)

    return node_evaluation(root, root.val, root.val)

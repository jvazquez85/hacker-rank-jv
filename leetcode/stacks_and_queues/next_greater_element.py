from typing import List


def next_greater_element(nums1: List[int], nums2: List[int]) -> List[int]:
    next_greater_map = {}
    stack = []

    for num in reversed(nums2):
        while stack and stack[-1] <= num:
            stack.pop()
        next_greater_map[num] = stack[-1] if stack else -1
        stack.append(num)

    return [next_greater_map[num] for num in nums1]
    # Initial approach
    # stack = [-1] * len(nums1)
    # queue = []
    #
    # for i in range(len(nums2) - 1, -1, -1):
    #     while queue and queue[-1] < nums2[i]:
    #         queue.pop()
    #     queue.append(nums2[i])
    #
    # for i, num in enumerate(nums1):
    #     for q in queue:
    #         if num < q:
    #             stack[i] = q
    #             break
    #
    # return stack

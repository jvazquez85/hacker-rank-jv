import unittest

from leetcode.stacks_and_queues.next_greater_element import next_greater_element


class TestNextGreaterElement(unittest.TestCase):
    def test_case_one(self):
        nums1 = [4, 1, 2]
        nums2 = [1, 3, 4, 2]
        expected = [-1, 3, -1]
        result = next_greater_element(nums1, nums2)
        self.assertEqual(expected, result)

    def test_case_two(self):
        nums1 = [2, 4]
        nums2 = [1, 2, 3, 4]
        expected = [3, -1]
        result = next_greater_element(nums1, nums2)
        self.assertEqual(expected, result)

    def test_case_three(self):
        nums1 = [1, 3, 5, 2, 4]
        nums2 = [6, 5, 4, 3, 2, 1, 7]
        expected = [7, 7, 7, 7, 7]
        result = next_greater_element(nums1, nums2)
        self.assertEqual(expected, result)

    def test_case_four(self):
        nums1 = [4, 1, 2]
        nums2 = [1, 3, 4, 2]
        expected = [-1, 3, -1]
        result = next_greater_element(nums1, nums2)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

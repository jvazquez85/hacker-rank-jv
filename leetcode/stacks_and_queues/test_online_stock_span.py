import unittest

from leetcode.stacks_and_queues.online_stock_span import StockSpanner


class TestOnlineStackSpan(unittest.TestCase):
    def test_case_one(self):
        spanner = StockSpanner()
        result = spanner.next(100)
        self.assertEqual(1, result)
        result = spanner.next(80)
        self.assertEqual(1, result)
        result = spanner.next(60)
        self.assertEqual(1, result)
        result = spanner.next(70)
        self.assertEqual(2, result)
        result = spanner.next(60)
        self.assertEqual(1, result)
        result = spanner.next(75)
        self.assertEqual(4, result)
        result = spanner.next(85)
        self.assertEqual(6, result)


if __name__ == "__main__":
    unittest.main()

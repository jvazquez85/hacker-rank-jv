from leetcode.challenges.solver import Solution


class IsPangram(Solution):
    @staticmethod
    def solve(sentence: str) -> bool:
        temp = set(sentence.lower())
        letters = set("abcdefghijklmnopqrstuvwxyz")
        return letters.issubset(temp)


if __name__ == "__main__":
    test_case = "thequickbrownfoxjumpsoverthelazydog"
    expected_solution = True
    assert expected_solution == IsPangram.solve(sentence=test_case)

from typing import List


class Solution:
    def solution(self, nums: List[int]) -> List[int]:
        def backtrack(curr: List[int]) -> None:
            if len(curr) == len(nums):
                ans.append(curr[:])
                return

            for num in nums:
                if num not in curr:
                    curr.append(num)
                    backtrack(curr)
                    curr.pop()

        ans = []
        backtrack([])
        return ans

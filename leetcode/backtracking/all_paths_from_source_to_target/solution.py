from typing import List


def all_path_source_target(graph: List[List[int]]) -> List[List[int]]:
    def backtrack(current_path: List[int], index: int) -> None:
        if index == len(graph) - 1:  # Termination when reaching the target node
            ans.append(list(current_path))
            return

        for next_node in graph[index]:
            current_path.append(next_node)
            backtrack(current_path, next_node)
            current_path.pop()

    ans = []
    backtrack([0], 0)
    return ans

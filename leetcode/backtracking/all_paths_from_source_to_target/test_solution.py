import unittest
from typing import List

from leetcode.backtracking.all_paths_from_source_to_target.solution import (
    all_path_source_target,
)


class TestAllPathSourceToTarget(unittest.TestCase):
    def test_something(self):
        graph: List[List[int]] = [[4, 3, 1], [3, 2, 4], [3], [4], []]
        expected: List[List[int]] = [
            [0, 4],
            [0, 3, 4],
            [0, 1, 3, 4],
            [0, 1, 2, 3, 4],
            [0, 1, 4],
        ]
        result = all_path_source_target(graph)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

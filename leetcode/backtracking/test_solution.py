import unittest
from typing import List

from leetcode.backtracking.solution import Solution


class MyTestCase(unittest.TestCase):
    def test_simple(self):
        nums: List[int] = [1, 2, 3]
        expected: List[List[int]] = [
            [1, 2, 3],
            [1, 3, 2],
            [2, 1, 3],
            [2, 3, 1],
            [3, 1, 2],
            [3, 2, 1],
        ]
        solution = Solution()
        result = solution.solution(nums)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

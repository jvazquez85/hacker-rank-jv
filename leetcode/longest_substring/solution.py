from collections import defaultdict


class Solution:
    def length_of_longest_substring(self, s: str) -> int:
        start = 0
        max_length = 1
        string_length = len(s)
        char_index_map = defaultdict(int)
        for i in range(string_length):
            if s[i] in char_index_map and char_index_map[s[i]] >= start:
                start = char_index_map[s[i]] + 1
            char_index_map[s[i]] = i
            max_length = max(max_length, i - start + 1)
        return max_length

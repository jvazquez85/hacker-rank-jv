import unittest

from leetcode.longest_substring.solution import Solution


class TestSolution(unittest.TestCase):
    def test_something(self):
        sample = "abcabcbb"
        expected = 3
        solution = Solution()
        self.assertEqual(expected, solution.length_of_longest_substring(sample))


if __name__ == "__main__":
    unittest.main()

import logging
import math
from os.path import abspath, dirname, join
from typing import List

import logconfig

from leetcode.challenges.solver import Solution

logconfig.from_json(abspath(join(dirname(__file__), "../../logging.json")))
logger = logging.getLogger("root")


class MaxSubarray(Solution):
    @staticmethod
    def solve(nums: List[int]) -> int:
        # Initialize our variables using the first element.
        current_subarray = max_subarray = nums[0]

        # Start with the 2nd element since we already used the first one.
        for num in nums[1:]:
            # If current_subarray is negative, throw it away.
            # Otherwise, keep adding to it.
            current_subarray = max(num, current_subarray + num)
            max_subarray = max(max_subarray, current_subarray)

        return max_subarray

    @staticmethod
    def last_personal_approach_solve(nums: List[int]) -> int:
        if len(nums) == 1:
            return nums[0]
        max_sum = -math.inf
        elements = len(nums)
        for i, number in enumerate(nums):
            j = elements
            while j > 0:
                if len(nums[i:j]) == 1 and nums[i:j][0] > max_sum:
                    max_sum = nums[i:j][0]
                elif len(nums[i:j]) > 1:
                    new_sum = sum(nums[i:j])
                    if new_sum > max_sum:
                        max_sum = new_sum
                j -= 1
        return max_sum

    @staticmethod
    def old_good_case_two(nums: List[int]):
        max_sum = -1000
        max_elements = len(nums)
        logger.info(f"Sequence is {nums}")
        for index, number in enumerate(nums):
            new_sum = sum(nums[index:max_elements])
            logger.info(f"New sum is {new_sum}. Max sum is {max_sum}. Number:{number}")
            if number >= new_sum and number > max_sum:
                max_sum = number
            elif new_sum >= max_sum:
                max_sum = new_sum
            # else:
            j_index = max_elements - 1
            logger.info(
                f"Pivot on {number} to last element at"
                f"{j_index} => {nums[index:j_index]}"
            )
            while j_index > 0 and j_index > index:
                new_sum = sum(nums[index:j_index])
                logger.info(f"New sequence {nums[index:j_index]}. New sum:{new_sum}")
                j_index -= 1
                if new_sum >= max_sum:
                    max_sum = new_sum
        return max_sum

    @staticmethod
    def old_good_case(nums: List[int]):
        # Solves until case 8
        max_sum = -100
        best_pivot = 0

        if len(nums) == 1:
            return nums[0]

        for index, number in enumerate(nums):
            new_sum = sum(nums[index : len(nums)])
            if number > new_sum and number > max_sum:
                max_sum = number
                best_pivot = index
            elif new_sum >= max_sum:
                max_sum = new_sum
                best_pivot = index

        # Now, invert it
        for index, number in enumerate(nums[best_pivot : len(nums)]):
            i = index
            if index == 0:
                i += 1

            if best_pivot != len(nums) - i:
                new_sum = sum(nums[best_pivot : len(nums) - i])
                if new_sum > max_sum:
                    max_sum = new_sum

        return max_sum

from collections import Counter


class Solution:
    @staticmethod
    def minimum_pushes(word: str) -> int:
        cnt = Counter(word)
        ans = 0
        for i, x in enumerate(sorted(cnt.values(), reverse=True)):
            print(f"i:{i} and x:{x}")
            ans += (i // 8 + 1) * x
        return ans
        # max_keys = 8  # Number of keys available for mapping (2 to 9)
        #
        # # If length of word is within the limit, return its length directly
        # if len(word) <= max_keys:
        #     return len(word)
        #
        # # Count the frequency of each character in the word
        # char_freq = Counter(word)
        #
        # # Sort characters by frequency in descending order
        # sorted_chars = sorted(char_freq.items(), key=lambda x: -x[1])
        # # Create a mapping to distribute characters over the keys
        # key_positions = {i: 0 for i in range(2, 10)}
        # total_presses = 0
        #
        # for char, freq in sorted_chars:
        #     # Find the key with the least load
        #     best_key = min(key_positions, key=key_positions.get)
        #     presses = key_positions[best_key] + 1
        #     total_presses += freq * presses
        #     key_positions[best_key] += 1
        #     if key_positions[best_key] >= 3:
        #         key_positions[best_key] = 0  # Reset position if it reaches 3
        #
        # return total_presses

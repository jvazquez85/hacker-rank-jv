import unittest

from leetcode.minimum_number_of_pushes_to_type_word_two.solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_first_case(self):
        word = "abcde"
        expected_result = 5
        result = self.solution.minimum_pushes(word)
        self.assertEqual(expected_result, result)

    def test_second_case(self):
        word = "xyzxyzxyzxyz"
        expected_result = 12
        result = self.solution.minimum_pushes(word)
        self.assertEqual(expected_result, result)

    def test_third_case(self):
        word = "aabbccddeeffgghhiiiiii"
        expected_result = 24
        result = self.solution.minimum_pushes(word)
        self.assertEqual(expected_result, result)

    def test_fourth_case(self):
        word = "bzimenxkcayfrduljp"
        expected_result = 30
        result = self.solution.minimum_pushes(word)
        self.assertEqual(expected_result, result)


if __name__ == "__main__":
    unittest.main()

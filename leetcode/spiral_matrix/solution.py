import logging
from os.path import abspath, dirname, join
from typing import List

import logconfig

from leetcode.challenges.solver import Solution

logconfig.from_json(abspath(join(dirname(__file__), "../../logging.json")))
logger = logging.getLogger("root")


class SpiralOrder(Solution):
    @staticmethod
    def solve(matrix: List[List[int]]) -> List[int]:
        rows = len(matrix) - 1
        columns = len(matrix[0]) - 1
        direction = 0
        top = 0
        left = 0
        right = columns
        bottom = rows
        result = []

        while top <= bottom and left <= right:
            if direction == 0:
                i = left
                while i <= right:
                    result.append(matrix[top][i])
                    i += 1
                top += 1
            elif direction == 1:
                for i in range(top, bottom + 1):
                    result.append(matrix[i][right])
                right -= 1
            elif direction == 2:
                i = right
                while i >= left:
                    result.append(matrix[bottom][i])
                    i -= 1
                bottom -= 1
            elif direction == 3:
                i = bottom
                while i >= top:
                    result.append(matrix[i][left])
                    i -= 1
                left += 1
            # Reminder of division
            direction = divmod(direction + 1, 4)[1]
        return result

    @staticmethod
    def brute_force(matrix: List[List[int]]) -> List[int]:
        rows = len(matrix) - 1
        columns = len(matrix[0]) - 1
        out = matrix[0]  # First row

        i = rows - 1
        while i <= rows:
            out.append(matrix[i][columns])
            i += 1

        j = columns - 1
        while j >= 0:
            out.append(matrix[rows][j])
            j -= 1

        i = rows - 1
        while i > 0:
            out.append(matrix[i][0])
            i -= 1

        j = columns - 1
        while j < columns:
            out.append(matrix[1][j])
            j += 1
        return out

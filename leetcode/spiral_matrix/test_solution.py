import unittest

from leetcode.spiral_matrix.solution import SpiralOrder


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {
                    "data": [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                    "expected": [1, 2, 3, 6, 9, 8, 7, 4, 5],
                    "name": "one",
                },
                {
                    "data": [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]],
                    "expected": [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7],
                    "name": "two",
                },
            ]
        }
        # for case in filter(lambda x: x.get("name") == "one", test_cases.get("cases")):
        for case in test_cases.get("cases"):
            result = SpiralOrder.solve(case.get("data"))
            self.assertEqual(
                case.get("expected"), result, f"Failed assertion on {case.get('name')}"
            )

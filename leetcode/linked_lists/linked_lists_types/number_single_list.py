from __future__ import annotations


class ListNode:
    def __init__(self, val: int = 0, next: ListNode | None = None):
        self.val = val
        self.next = next

    @staticmethod
    def add_node(head: ListNode | None, next_node_value: int):
        new_node = ListNode(val=next_node_value)
        if head is None:
            head = new_node
        else:
            curr = head
            while curr.next is not None:
                curr = curr.next
            curr.next = new_node

    @staticmethod
    def pop_node(head: ListNode | None, target: int) -> ListNode:
        # Check if it's the head node
        while head is not None and head.val == target:
            head = head.next

        current = head
        prev = None

        # Traverse the linked list to find the element to be removed
        while current is not None:
            if current.val == target:
                # Remove the element by bypassing the current node
                prev.next = current.next
                current = current.next
            else:
                prev = current
                current = current.next

        return head

    def __str__(self) -> str:
        result = []
        curr = self
        while curr is not None:
            result.append(str(curr.val))
            curr = curr.next
        return " -> ".join(result)

    def __eq__(self, other: ListNode) -> bool:
        curr1 = self
        curr2 = other

        while curr1 is not None and curr2 is not None:
            if curr1.val != curr2.val:
                return False
            curr1 = curr1.next
            curr2 = curr2.next

        return curr1 is None and curr2 is None

from __future__ import annotations

from typing import Optional

from leetcode.challenges.solver import Solution
from leetcode.linked_lists.linked_lists_challenges.number_single_list import ListNode


class MiddleOfLinkedListSlice(Solution):
    @staticmethod
    def solve(linked_list: Optional[ListNode]) -> ListNode | None:
        slow = linked_list
        fast = linked_list

        while fast is not None and fast.next is not None:
            slow = slow.next
            fast = fast.next.next

        return slow


if __name__ == "__main__":
    samples = [number for number in range(1, 7)]
    test_case_node = ListNode(val=samples[0])

    for number in samples[1:]:
        ListNode.add_node(test_case_node, number)

    solution_node = ListNode(val=samples[3:][0])
    for number in samples[4:]:
        ListNode.add_node(solution_node, number)

    result = MiddleOfLinkedListSlice.solve(linked_list=test_case_node)
    assert (
        result == solution_node
    ), f"Expected to obtain {solution_node}, but got {result}"

from typing import Required

from leetcode.challenges.solver import Solution
from leetcode.linked_lists.linked_lists_types.number_single_list import ListNode


class DeleteDuplicates(Solution):
    @staticmethod
    def solve(head: Required[ListNode]) -> ListNode:
        current = head

        while current is not None:
            # Use a separate pointer to traverse the rest of the list
            runner = current
            while runner.next is not None:
                if runner.next.val == current.val:
                    # Remove the duplicate node by bypassing it
                    runner.next = runner.next.next
                else:
                    runner = runner.next

            current = current.next

        return head


if __name__ == "__main__":
    samples = [number for number in [1, 1, 2, 3, 3]]
    test_case_node = ListNode(val=samples[0])

    for number in samples[1:]:
        ListNode.add_node(test_case_node, number)

    solution = ListNode(val=1)
    ListNode.add_node(solution, 2)
    ListNode.add_node(solution, 3)

    result = DeleteDuplicates.solve(head=test_case_node)
    print(samples)
    print(result)
    # assert result == solution, f"Expected to obtain {solution}, but got {result}"

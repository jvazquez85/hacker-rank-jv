import unittest

from linked_lists_types.number_single_list import ListNode


class TestListOperations(unittest.TestCase):
    def test_remove_node(self):
        test_list = ListNode(val=1)
        ListNode.add_node(test_list, 2)
        ListNode.add_node(test_list, 3)
        result = ListNode.pop_node(test_list, 2)

        expected_solution = ListNode(val=1)
        ListNode.add_node(expected_solution, 3)
        assert expected_solution == result, (
            f"Linked lists are different.\n"
            f"Expected {expected_solution}\n"
            f"Got {result}"
        )

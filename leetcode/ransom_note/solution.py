from collections import Counter


def can_construct(ransom_note: str, magazine: str) -> bool:
    if len(ransom_note) > len(magazine):
        return False

    frequency_magazine = Counter(magazine)
    frequency_ransom = Counter(ransom_note)
    print(f"Magazine: {frequency_magazine} -> Ransom: {frequency_ransom}")
    for char in ransom_note:
        if (
            frequency_magazine[char] < frequency_ransom[char]
            or char not in frequency_magazine
        ):
            return False

    return True

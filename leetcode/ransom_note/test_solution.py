import unittest

from leetcode.ransom_note.solution import can_construct


class TestCanConstruct(unittest.TestCase):
    def test_case_example(self):
        ransom_note = "aa"
        magazine = "ab"
        result = can_construct(ransom_note, magazine)
        self.assertEqual(False, result)


if __name__ == "__main__":
    unittest.main()

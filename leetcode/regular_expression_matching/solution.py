class Solution:
    def is_match(self, subject: str, pattern: str) -> bool:
        def backtrack(curr_subject: str, curr_pattern: str) -> bool:
            if not curr_pattern:
                print("\tearly leave")
                return not curr_subject

            first_match = bool(curr_subject) and curr_pattern[0] in {
                curr_subject[0],
                ".",
            }
            if len(curr_pattern) >= 2 and curr_pattern[1] == "*":
                return backtrack(curr_subject, curr_pattern[2:]) or (
                    first_match and backtrack(curr_subject[1:], curr_pattern)
                )
            else:
                return first_match and backtrack(curr_subject[1:], curr_pattern[1:])

        return backtrack(subject, pattern)

import unittest

from leetcode.regular_expression_matching.solution import Solution


class MyTestCase(unittest.TestCase):
    def test_simple(self):
        subject = "aa"
        pattern = "a*"
        solution = Solution()
        result = solution.is_match(subject=subject, pattern=pattern)
        expected = True
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

import unittest

from leetcode.remove_stones.solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_case_example(self):
        piles = [5, 4, 9]
        k = 2
        self.assertEqual(12, self.solution.min_stone_sum(piles=piles, k=k))

    def test_case_example_two(self):
        piles = [4, 3, 6, 7]
        k = 3
        self.assertEqual(12, self.solution.min_stone_sum(piles=piles, k=k))


if __name__ == "__main__":
    unittest.main()

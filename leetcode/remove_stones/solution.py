import heapq
import math
from typing import List


class Solution:
    def min_stone_sum(self, piles: List[int], k: int) -> int:
        max_heap = [-x for x in piles]
        heapq.heapify(max_heap)

        for _ in range(k):
            largest = -heapq.heappop(max_heap)
            reduced = largest - math.floor(largest / 2)
            heapq.heappush(max_heap, -reduced)

        return -sum(max_heap)

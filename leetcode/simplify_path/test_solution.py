import unittest

from leetcode.simplify_path.solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_case_one(self):
        path = "/home//foo/"
        expected_result = "/home/foo"
        self.assertEqual(expected_result, self.solution.simplify_path(path))


if __name__ == "__main__":
    unittest.main()

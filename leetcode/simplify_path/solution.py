class Solution:
    def simplify_path(self, path: str) -> str:
        stack = []
        segment = []

        for char in path:
            if char == "/":
                if segment:
                    segment_str = "".join(segment)
                    if segment_str == "..":
                        if stack:
                            stack.pop()
                    elif segment_str != "." and segment_str:
                        stack.append(segment_str)
                    segment = []  # Reset segment after processing
            else:
                segment.append(char)

        if segment:
            segment_str = "".join(segment)
            if segment_str == "..":
                if stack:
                    stack.pop()
            elif segment_str != "." and segment_str:
                stack.append(segment_str)

        return "/" + "/".join(stack)

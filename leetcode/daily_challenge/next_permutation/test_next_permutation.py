import unittest

from .solution import next_permutation


class NextPermutationTest(unittest.TestCase):
    def test_case_example(self):
        nums = [1, 2, 3]
        expected = [3, 2, 1]
        next_permutation(nums)
        self.assertEqual(expected, nums)

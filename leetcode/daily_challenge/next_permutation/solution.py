from typing import List


def next_permutation(nums: List[int]) -> None:
    n = len(nums)
    if n <= 1:
        return

    # Step 1: Find the pivot
    pivot = -1
    i = n - 2  # The second to last part of the array
    while i >= 0:
        if nums[i] < nums[i + 1]:
            pivot = i
            break
        i -= 1

    if pivot == -1:
        # If no pivot, the array is in descending order
        nums.reverse()
        return

    # Step 2: Find the successor
    for i in range(n - 1, pivot, -1):
        if nums[i] > nums[pivot]:
            # Step 3: Swap pivot and successor
            nums[i], nums[pivot] = nums[pivot], nums[i]
            break

    # Step 4: Reverse the suffix
    nums[pivot + 1 :] = reversed(nums[pivot + 1 :])

import unittest
from typing import List

from leetcode.prefix_sum.example import answer_queries


class TestPrefixSum(unittest.TestCase):
    def test_prefix_sum(self):
        nums: List[int] = [1, 6, 3, 2, 7, 2]
        queries: List[List[int]] = [[0, 3], [2, 5], [2, 4]]
        limit: int = 13
        expected: List[bool] = [True, False, True]
        self.assertEqual(expected, answer_queries(nums, queries, limit))


if __name__ == "__main__":
    unittest.main()

from typing import List


def answer_queries(nums: List[int], queries: List[List[int]], limit: int) -> List[bool]:
    prefix = [nums[0]]
    for i in range(1, len(nums)):
        print(f"i {i} prefix[-1] {prefix[-1]}")
        prefix.append(nums[i] + prefix[-1])

    ans = []
    for x, y in queries:
        curr = prefix[y] - prefix[x] + nums[x]
        ans.append(curr < limit)

    return ans

import logging
from os.path import abspath, dirname, join
from typing import List

import logconfig

from leetcode.challenges.solver import Solution

logconfig.from_json(abspath(join(dirname(__file__), "../../logging.json")))
logger = logging.getLogger("root")


class MergeSortedArray(Solution):
    @staticmethod
    def solve(nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        # To accommodate this, nums1 has a length of m + n
        if len(nums1) != m + n:
            return

        if len(nums2) == 0:
            return

        if len(nums2) == 1 and len(nums1) == 0:
            nums1 = nums2

        for i in nums2:
            nums1[nums1.index(0)] = i
        nums1.sort()

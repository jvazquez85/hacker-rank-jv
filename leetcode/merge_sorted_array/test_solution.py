import unittest

from leetcode.merge_sorted_array.solution import MergeSortedArray


class TestSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_cases(self):
        test_cases = {
            "cases": [
                {
                    "data": {
                        "nums1": [1, 2, 3, 0, 0, 0],
                        "m": 3,
                        "nums2": [2, 5, 6],
                        "n": 3,
                    },
                    "expected": [1, 2, 2, 3, 5, 6],
                    "name": "one",
                }
            ]
        }
        # for case in filter(lambda x: x.get("name") == "one", test_cases.get("cases")):
        for case in test_cases.get("cases"):
            MergeSortedArray.solve(**case.get("data"))
            self.assertEqual(
                case.get("expected"),
                case.get("data").get("nums1"),
                f"Failed assertion on {case.get('name')}",
            )

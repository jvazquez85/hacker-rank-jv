from typing import List

from leetcode.challenges.solver import Solution


class LongestConsecutiveSequence(Solution):
    @staticmethod
    def solve(nums: List[int]) -> int:
        num_set = set(nums)
        longest_streak = 0

        for num in num_set:
            if (
                num - 1 not in num_set
            ):  # Check if num is the starting point of a sequence
                current_num = num
                current_streak = 1

                while current_num + 1 in num_set:  # Expand the sequence
                    current_num += 1
                    current_streak += 1

                longest_streak = max(longest_streak, current_streak)

        return longest_streak


if __name__ == "__main__":
    case_one = [100, 4, 200, 1, 3, 2]
    solution = LongestConsecutiveSequence.solve(nums=case_one)
    expected = 4
    assert solution == expected, f"Expected {expected}, got {solution}"

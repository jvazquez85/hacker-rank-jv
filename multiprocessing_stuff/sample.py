from multiprocessing import Array, Process, Value


def f(number: Value, array: Array):
    number.value = 3.1415927
    for i in range(len(array)):
        array[i] = -array[i]


if __name__ == "__main__":
    num = Value("d", 0.0)
    arr = Array("i", range(10))

    p = Process(target=f, args=(num, arr))
    p.start()
    p.join()

    print(num.value)
    print(arr[:])

from typing import Dict, List


def get_mapping() -> Dict:
    return {"A": "00", "T": "01", "C": "10", "G": "11"}


def sequence_to_int(sequence: str) -> int:
    mapping = get_mapping()
    binary_representation = "".join([mapping[char] for char in sequence])
    return int(binary_representation, 2)


def int_to_sequence(num: int, k: int) -> str:
    mapping = get_mapping()
    inverse_mapping = {
        int(v, 2): k for k, v in mapping.items()
    }  # Adjusting inverse_mapping
    sequence = []
    for _ in range(k):
        two_bits = num & 0b11  # this will give us integer representation
        sequence.append(inverse_mapping[two_bits])
        num >>= 2
    return "".join(reversed(sequence))


def find_repeated_sequences(s: str, k: int) -> List[str]:
    sequences = {}
    right = k
    for left in range(len(s)):
        sequence = s[left:right]
        if len(sequence) < k:
            break
        seq = sequence_to_int(sequence)
        if seq in sequences.keys():
            sequences[seq] += 1
        else:
            sequences[seq] = 1
        right += 1

    result_keys = {key for key, value in sequences.items() if value > 1}
    if len(result_keys) == 0:
        return []
    return [int_to_sequence(seq, k) for seq in result_keys]

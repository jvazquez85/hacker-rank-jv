import unittest

from educative.sliding_window.repeated_dna_sequence.app import find_repeated_sequences


class TestRepeatedSequencesTestCase(unittest.TestCase):
    def test_case_one(self):
        input = "AAAAACCCCCAAAAACCCCCC"
        k = 8
        expected = ["AAAAACCC", "AAAACCCC", "AAACCCCC"]
        result = find_repeated_sequences(input, k)
        self.assertEqual(
            sorted(expected), sorted(result), f"Expected {expected}, got {result}"
        )


if __name__ == "__main__":
    unittest.main()

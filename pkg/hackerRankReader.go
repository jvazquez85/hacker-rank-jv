package pkg

import (
	"bufio"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

// LineExtractor will
func LineExtractor(peopleInLine int, lineOfPeople []string) []int32 {
	var peopleLine []int32
	for i := 0; i < peopleInLine; i++ {
		qItemTemp, err := strconv.ParseInt(lineOfPeople[i], 10, 64)
		checkError(err)
		person := int32(qItemTemp)
		peopleLine = append(peopleLine, person)
	}
	return peopleLine
}

// ObtainReader will obtain the reader
func ObtainReader(testCasePath string) *bufio.Reader {
	fixture, err := os.Open(testCasePath)
	defer func(fixture *os.File) {
		err := fixture.Close()
		if err != nil {
			log.Fatalf("%e", err)
		}
	}(fixture)
	checkError(err)
	return bufio.NewReaderSize(fixture, 1024*1024)
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func ReadLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

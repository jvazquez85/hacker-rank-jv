import unittest

from hackerrank.fraudulent_activity.app import activity_notifications


class TestFraudulentActivity(unittest.TestCase):
    def test_activity_notifications_first(self):
        d = 3
        expenditure = [10, 20, 30, 40, 50]
        expected = 1
        notifications = activity_notifications(expenditure=expenditure, d=d)
        self.assertEqual(notifications, expected)


if __name__ == "__main__":
    unittest.main()

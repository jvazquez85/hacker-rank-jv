from typing import List


def calculate_median(window: List[int]) -> float:
    sorted_window = sorted(window)
    n = len(sorted_window)
    if n % 2 == 1:
        return sorted_window[n // 2]
    else:
        return (sorted_window[n // 2 - 1] + sorted_window[n // 2]) / 2


def activity_notifications(expenditure: List[int], d: int) -> int:
    count = 0
    window = expenditure[:d]
    total_movements = len(expenditure)
    for i in range(d, total_movements):
        median = calculate_median(window)
        if expenditure[i] >= 2 * median:
            count += 1
        window.pop(0)
        window.append(expenditure[i])
    return count

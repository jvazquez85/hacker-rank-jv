import unittest

from hackerrank.sherlock_and_the_valid_string.solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_example_method(self):
        s = "abc"
        expected = "YES"
        self.assertEqual(self.solution.is_valid(s), expected)

    def test_example_no(self):
        s = "abccc"
        expected = "NO"
        self.assertEqual(self.solution.is_valid(s), expected)

    def test_failing_test_case(self):
        s = "aabbccddeefghi"
        expected = "NO"
        self.assertEqual(self.solution.is_valid(s), expected)

    def test_failing_test_case2(self):
        s = "abcdefghhgfedecba"
        expected = "YES"
        self.assertEqual(self.solution.optimized_is_valid(s), expected)


if __name__ == "__main__":
    unittest.main()

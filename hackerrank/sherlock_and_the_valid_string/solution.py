from collections import Counter


class Solution:
    def is_valid(self, s: str) -> str:
        frequency = {}

        for letter in s:
            if letter in frequency:
                frequency[letter] += 1
            else:
                frequency[letter] = 1

        reduced_frequency = set(frequency.values())
        if len(reduced_frequency) == 1:
            return "YES"

        if len(reduced_frequency) % 2 != 0:
            # No because I'm getting odd freqs, doing a subtraction won't help
            return "NO"

        print(
            f"what is reduced_freq then {reduced_frequency}, and frequency: {frequency}"
        )
        # Simple final check
        freq1, freq2 = list(reduced_frequency)
        count1, count2 = list(frequency.values()).count(freq1), list(
            frequency.values()
        ).count(freq2)

        # Check if one frequency can be reduced to the other
        if (count1 == 1 and (freq1 - 1 == freq2 or freq1 - 1 == 0)) or (
            count2 == 1 and (freq2 - 1 == freq1 or freq2 - 1 == 0)
        ):
            return "YES"

        return "NO"

    def optimized_is_valid(self, s: str) -> str:
        # Step 1: Count frequency of each character
        frequency = Counter(s)

        # Step 2: Count frequency of frequencies
        freq_count = Counter(frequency.values())

        # Step 3: If there's only one unique frequency, it's valid
        if len(freq_count) == 1:
            return "YES"

        # Step 4: If there are more than two unique frequencies, it's invalid
        if len(freq_count) > 2:
            return "NO"

        # Step 5: There are exactly two unique frequencies
        (freq1, count1), (freq2, count2) = freq_count.items()

        # Step 6: Check if one frequency can be reduced to the other
        if (count1 == 1 and (freq1 - 1 == freq2 or freq1 - 1 == 0)) or (
            count2 == 1 and (freq2 - 1 == freq1 or freq2 - 1 == 0)
        ):
            return "YES"

        return "NO"

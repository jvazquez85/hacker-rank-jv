Problem Statement Analysis

"Davis has a number of staircases in his house, and he likes to climb each staircase 1, 2, or 3 steps at a time. Being a very precocious child, he wonders how many ways there are to reach the top of each staircase. Given the respective heights for each of the s staircases in his house, find and print the number of ways he can climb each staircase, modulo 1010+71010+7, on a new line."
Key Points to Understand

    Number of Staircases (s):
        The first value in the input specifies the number of staircases.
    Heights of Staircases:
        The subsequent s values represent the heights of each staircase.

Example Provided

Example Input: 3 1 3 7

    The first number (3) indicates there are 3 staircases.
    The next numbers (1, 3, 7) are the heights of the staircases.

from typing import List, Tuple


def count_swaps(a: List) -> Tuple[int, int, int]:
    swaps = 0
    for _ in range(len(a)):
        for j in range(0, len(a) - 1):
            if a[j] > a[j + 1]:
                a[j + 1], a[j] = a[j], a[j + 1]
                swaps += 1
    return swaps, a[0], a[-1]

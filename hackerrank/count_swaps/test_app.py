import unittest

from hackerrank.count_swaps.app import count_swaps


class CountSwapsTestCase(unittest.TestCase):
    def test_count_swaps_base_test_case(self):
        expected = (3, 1, 6)
        result = count_swaps(a=[6, 4, 1])
        self.assertEqual(
            expected, result, f"Expected {expected} but got {result}"
        )  # add assertion here


if __name__ == "__main__":
    unittest.main()

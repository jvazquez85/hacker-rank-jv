import unittest

from hackerrank.graphs.find_nearest_clone import find_shortest


class TestFindNearestClone(unittest.TestCase):
    def test_case_zero(self):
        graph_nodes = 4  # Number of nodes
        graph_from = [1, 1, 4]  # Start nodes
        graph_to = [2, 3, 2]  # End nodes
        color_list = [1, 2, 1, 1]
        # Target color value
        target_color = 1
        expected_output = 1
        solution = find_shortest(
            graph_nodes=graph_nodes,
            graph_from=graph_from,
            graph_to=graph_to,
            color_list=color_list,
            target_color=target_color,
        )
        self.assertEqual(solution, expected_output, "expected solution not found")

    def test_case_two(self):
        graph_nodes = 5
        graph_from = [1, 1, 2, 3]
        graph_to = [2, 3, 4, 5]
        color_list = [1, 2, 3, 3, 2]
        target_color = 2
        expected_output = 3
        solution = find_shortest(
            graph_nodes=graph_nodes,
            graph_from=graph_from,
            graph_to=graph_to,
            color_list=color_list,
            target_color=target_color,
        )
        self.assertEqual(solution, expected_output, "expected solution not found")


if __name__ == "__main__":
    unittest.main()

# Roads and libraries

Determine the minimum cost to provide library access to
all citizens of HackerLand. There are n cities numbered from 1 to n.

Currently, there are no libraries and the cities are not connected.
Bidirectional roads may be built between any city pair listed in
cities.

A citizen has access to a library if:

- Their city contains a library
- They can travel by road from their city to a city containing a library.

Example:

The following figure is a sample map of HackerLand where the dotted lines
denote possible roads:

![img.png](img.png)

```python
c_road = 2
c_lib = 3
cities = [[1, 7], [1, 3], [1, 2], [2, 3], [5, 6], [6, 8]]
```

The cost of building any road is

```python
c_road = 2
```

And the cost to build a library in any city is

```python
c_lib = 3
```

Build 5 roads at a cost of 5 x 2 = 10 and 2 libraries for a cost of 6.
One of the available roads in the cycle
1 -> 2 -> 3 -> 1 is not necessary.

There are q queries, where each query consist of a map of HackerLand and value of `c_lib` and `c_road`.
For each query, find the minimum cost to make libraries accessible to all the citizens.

## Function Description

Complete the function roadsAndLibraries in the editor below.
roadsAndLibraries has the following parameters:
- int `n`: integer, the number of cities
- int `c_lib`: integer, the cost to build a library
- int `c_road`: integer, the cost to repair a road
- int `cities[m][2]`: each `cities[i]` contains two integers that represent cities that can be connected by a new road

### Returns

- int: the minimal cost

## Input Format

The first line contains a single integer q, that denotes the number of queries.
The subsequent lines describe each query in the following format:
- The first line contains four space separated integers that describe the respective values of
  `n, m, c_lib` and `c_road`, the number of cities, number of roads, cost of a library and a cost of a road.
- Each of the next `m` lines contains two space-separated integers, `u[i]` and `v[i]`, that describe a bidirectional
road that can be built to connect cities `u[i]` and `v[i]`.

## Constraints

- 1 <= q <= 10
- 1 <= n <= 10^5
- 0 <= m <= min(10^5, n*(n-1) / 2)
- 1 <= c_road, c_lib < 10 ^5
- 1 <= u[i], v[i] <= n
- Each road connects two distinct cities

![img_1.png](img_1.png)

See the rest of this crap [here](https://www.hackerrank.com/challenges/torque-and-development/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=graphs)


# Observations

- There is a clear typo in the variables where they declare c_roads and then rename it cc_roads.
- It is clearly written in a way that is confusing, so the first thing I need to do, is to clarify the input.
- The prototype of the function is a mess, it does not follow the same format that they describe in input format

```
n, m, c_lib and c_road, the number of cities, number of roads, cost of a library and a cost of a road.
def roadsAndLibraries(n, c_lib, c_road, cities):
    # Write your code here
```

Clearly that is just wrong, first the variables are not even descriptive, the function name is not using pep8
and the arguments are swapped, I don't know if it is a clear error, or just adding fuzziness to confuse the person
solving this.

First, I'll rewrite the input because it's not clear.
input

`
2
3 3 2 1
1 2
3 1
2 3
`

Properly translated

```
queries = 2
number_of_cities = 3
number_of_roads = 3
library_cost = 2
road_cost = 1
graph = [[1, 2], [3, 1], [2, 3]]
```

You need to perform two queries, and the total output is 4

They also cram a second example there that is equally confusing

Explaining example one.

- Build a library on city 1 at a cost of 2
- Build the road between cities 1 and 2 at a cost of 1
- Build the road between cities 2 and 3 at a cost of 1
- You do not need to build the road between 1 and 3, because they are connected by city 2

They also provide you 2 different scenarios per test case, so you need to calculate both.

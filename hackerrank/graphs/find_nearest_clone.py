import pprint
from collections import defaultdict, deque
from typing import List

pp = pprint.PrettyPrinter(indent=4)


def find_shortest(
    graph_nodes: int,
    graph_from: List[int],
    graph_to: List[int],
    color_list: List[int],
    target_color: int,
) -> int:
    # Build the adjacency list
    adjacency_map = defaultdict(list)
    for u, v in zip(graph_from, graph_to):
        adjacency_map[u].append(v)
        adjacency_map[v].append(u)

    # Identify all nodes with the target color
    target_nodes = [i + 1 for i in range(graph_nodes) if color_list[i] == target_color]

    # If there are less than 2 nodes with the target color, return -1
    if len(target_nodes) < 2:
        return -1

    # Perform BFS from all target nodes simultaneously
    shortest_path = float("inf")

    for start_node in target_nodes:
        queue = deque([(start_node, 0)])
        visited = {start_node}

        while queue:
            current, distance = queue.popleft()
            for neighbor in adjacency_map[current]:
                if neighbor not in visited:
                    if color_list[neighbor - 1] == target_color:
                        shortest_path = min(shortest_path, distance + 1)
                    visited.add(neighbor)
                    queue.append((neighbor, distance + 1))

    return shortest_path if shortest_path != float("inf") else -1

from typing import Dict, List

from utils.graphs.helper import deep_first_search, to_adjacency_map


def find_connected_components_using_deep_first_search(
    adjacency_map: Dict[int, List[int]]
) -> List[List[int]]:
    visited = set()
    components = []
    for node in adjacency_map:
        if node not in visited:
            component = deep_first_search(node, adjacency_map, visited)
            components.append(component)
    return components


def roads_and_libraries(
    number_of_cities: int,
    library_cost: int,
    road_cost: int,
    city_graph: List[List[int]],
) -> int:
    adjacency_map = to_adjacency_map(city_graph, number_of_cities)
    components = find_connected_components_using_deep_first_search(adjacency_map)
    total_cost = 0

    for component in components:
        num_cities_in_component = len(component)
        cost_with_one_library = library_cost + (num_cities_in_component - 1) * road_cost
        cost_with_libraries_in_all_cities = num_cities_in_component * library_cost
        total_cost += min(cost_with_one_library, cost_with_libraries_in_all_cities)

    return total_cost

import unittest

from roads_and_libraries import roads_and_libraries, to_adjacency_map

from utils.graphs.helper import deep_first_search


class TestRoadsAndLibraries(unittest.TestCase):
    def test_to_adjacency_map(self):
        city_graph = [[1, 2], [3, 1], [2, 3]]
        result = to_adjacency_map(city_graph)
        print(result)

    def test_deep_first_search(self):
        city_graph = [[1, 2], [3, 1], [2, 3]]
        adjacency_map = to_adjacency_map(city_graph)
        visited = set()
        components = []
        for node in adjacency_map:
            if node not in visited:
                component = deep_first_search(node, adjacency_map, visited)
                components.append(component)
        print(components)

    def test_case_example_one(self):
        expected_best_cost = 4
        number_of_cities = 3
        library_cost = 2
        road_cost = 1
        city_graph = [[1, 2], [3, 1], [2, 3]]
        best_cost = roads_and_libraries(
            number_of_cities=number_of_cities,
            library_cost=library_cost,
            road_cost=road_cost,
            city_graph=city_graph,
        )
        self.assertEqual(
            expected_best_cost,
            best_cost,
            "There is a difference between expected_best_cost and best_cost",
        )

    def test_case_example_two(self):
        expected_best_cost = 12
        number_of_cities = 6
        library_cost = 2
        road_cost = 5
        city_graph = [[1, 3], [3, 4], [2, 4], [1, 2], [2, 3], [5, 6]]
        best_cost = roads_and_libraries(
            number_of_cities=number_of_cities,
            library_cost=library_cost,
            road_cost=road_cost,
            city_graph=city_graph,
        )
        self.assertEqual(
            expected_best_cost,
            best_cost,
            "There is a difference between expected_best_cost and best_cost",
        )

    def test_case_number_two(self):
        expected_best_cost = 15
        number_of_cities = 5
        library_cost = 6
        road_cost = 1
        city_graph = [[1, 2], [1, 3], [1, 4]]
        best_cost = roads_and_libraries(
            number_of_cities=number_of_cities,
            library_cost=library_cost,
            road_cost=road_cost,
            city_graph=city_graph,
        )
        self.assertEqual(
            expected_best_cost,
            best_cost,
            "There is a difference between expected_best_cost and best_cost",
        )

import unittest

from hackerrank.two_strings.app import (
    DO_NOT_SHARE_SUBSTRING,
    SHARE_SUBSTRING,
    twoStrings,
)


class TestTwoStrings(unittest.TestCase):
    def test_first(self):
        result = twoStrings("hello", "world")
        self.assertEqual(SHARE_SUBSTRING, result)
        result = twoStrings("hi", "world")
        self.assertEqual(DO_NOT_SHARE_SUBSTRING, result)


if __name__ == "__main__":
    unittest.main()

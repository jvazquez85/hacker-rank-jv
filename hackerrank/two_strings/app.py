SHARE_SUBSTRING = "YES"
DO_NOT_SHARE_SUBSTRING = "NO"


def twoStrings(s1, s2):
    return (
        SHARE_SUBSTRING
        if len(set(s1).intersection(set(s2))) > 0
        else DO_NOT_SHARE_SUBSTRING
    )


if __name__ == "__main__":
    twoStrings("hello", "world")

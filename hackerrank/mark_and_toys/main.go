package main

import (
	"log"
	"sort"
)

type toys []int32

func (collection toys) Len() int {
	return len(collection)
}

func (collection toys) Less(i, j int) bool {
	return collection[i] < collection[j]
}

func (collection toys) Swap(i, j int) {
	collection[i], collection[j] = collection[j], collection[i]
}

func maximumToys(toyPrices toys, budget int32) int32 {
	var totalPrice, totalToys, iBudget int
	iBudget = int(budget)

	sort.Sort(toyPrices)
	for i := 0; i < len(toyPrices); i++ {
		if totalPrice+int(toyPrices[i]) <= iBudget {
			totalPrice += int(toyPrices[i])
			totalToys += 1
		}
	}
	return int32(totalToys)
}

func main() {
	var budget int32 = 50
	toyPrices := []int32{1, 12, 5, 111, 200, 1000, 10}
	caseOne := maximumToys(toyPrices, budget)
	caseOneExpected := 4
	if caseOne != 4 {
		log.Fatalf("Unexpected output. %d is not the testcase one (%d) expected value.\n", caseOne,
			caseOneExpected)
	}
}

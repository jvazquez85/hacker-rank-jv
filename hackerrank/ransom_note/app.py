from collections import Counter
from typing import List

YES = "Yes"
NO = "No"


def checkMagazine(magazine: List[str], note: List[str]):
    word_count = Counter(magazine)

    # Check each word in the note
    for word in note:
        if word in word_count and word_count[word] > 0:
            word_count[word] -= 1
        else:
            return NO

    return YES

import os.path
import unittest

from hackerrank.ransom_note.app import NO, YES, checkMagazine
from hackerrank.utils.test_case_opener import test_case_opener


class TestCheckMagazine(unittest.TestCase):
    def test_something(self):
        response = checkMagazine(
            "give me one grand today night".split(" "),
            "give one grand today".split(" "),
        )
        self.assertEqual(YES, response, f"Expected {YES} but got {response}")

    def test_sample_test_case_one(self):
        response = checkMagazine(
            "two times three is not four".split(" "), "two times two is four".split(" ")
        )
        self.assertEqual(NO, response, f"Expected {NO} but got {response}")

    def test_sample_test_case_two(self):
        response = checkMagazine(
            "ive got a lovely bunch of coconuts", "ive got some coconuts"
        )
        self.assertEqual(
            NO, response, f"Expected {NO} but got {response} on coconut case"
        )

    def test_sample_test_case_thirteen(self):
        sample_path = os.path.join(os.path.dirname(__file__), "test_case_thirteen.md")
        magazine, note = test_case_opener(sample_path)
        response = checkMagazine(magazine=magazine, note=note)
        self.assertEqual(YES, response, f"Expected {YES} but got {response}")

    def test_sample_test_case_sixteen(self):
        sample_path = os.path.join(os.path.dirname(__file__), "test_case_sixteen.md")
        magazine, note = test_case_opener(sample_path)
        response = checkMagazine(magazine=magazine, note=note)
        self.assertEqual(YES, response, f"Expected {YES} but got {response}")


if __name__ == "__main__":
    unittest.main()

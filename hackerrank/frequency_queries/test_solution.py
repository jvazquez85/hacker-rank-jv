import unittest
from typing import List, Tuple

from hackerrank.frequency_queries.solution import freq_query


class TestSolution(unittest.TestCase):
    def test_simple(self):
        queries: List[Tuple[int, int]] = [
            (1, 1),
            (2, 2),
            (3, 2),
            (1, 1),
            (1, 1),
            (2, 1),
            (3, 2),
        ]
        expected: List[int] = [0, 1]
        result: List[int] = freq_query(queries)
        self.assertEqual(expected, result)

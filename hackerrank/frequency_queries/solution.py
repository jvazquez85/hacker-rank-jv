from collections import defaultdict
from typing import List, Tuple

# We remove the full scan because on a 10^5 that full count takes a shit ton of time


def freq_query(queries: List[Tuple[int, int]]) -> List[int]:
    element_frequency = defaultdict(int)
    frequency_count = defaultdict(int)
    results = []

    for operation, value in queries:
        if operation == 1:
            current_freq = element_frequency[value]
            element_frequency[value] += 1
            new_freq = element_frequency[value]
            if current_freq > 0:
                frequency_count[current_freq] -= 1
            frequency_count[new_freq] += 1

        elif operation == 2:
            if element_frequency[value] > 0:
                current_freq = element_frequency[value]
                element_frequency[value] -= 1
                new_freq = element_frequency[value]
                frequency_count[current_freq] -= 1
                if new_freq > 0:
                    frequency_count[new_freq] += 1

        elif operation == 3:
            results.append(1 if frequency_count[value] > 0 else 0)

    return results

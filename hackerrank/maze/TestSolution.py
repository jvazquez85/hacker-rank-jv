import unittest

from hackerrank.maze.solution import decide_move


class TestSolution(unittest.TestCase):
    def test_example_with_exit(self):
        _partial_map = ["#-#", "#--", "-e-"]
        self.assertEqual("DOWN", decide_move(_partial_map))

    def test_example_without_exit(self):
        _partial_map = ["#-#", "#--", "---"]
        self.assertEqual("DOWN", decide_move(_partial_map))


if __name__ == "__main__":
    unittest.main()

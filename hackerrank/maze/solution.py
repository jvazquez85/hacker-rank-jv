from collections import deque


def determine_direction(position, target):
    vertical_move = target[0] - position[0]
    horizontal_move = target[1] - position[1]

    if vertical_move == 1:
        return "DOWN"
    elif vertical_move == -1:
        return "UP"
    elif horizontal_move == 1:
        return "RIGHT"
    elif horizontal_move == -1:
        return "LEFT"


def bfs(start, adjacency_map):
    queue = deque([start])
    visited = set([start])

    while queue:
        position = queue.popleft()

        for path in adjacency_map[position]:
            new_position = (path[0], path[1])
            if new_position not in visited:
                visited.add(new_position)
                if path[2] == 1:  # Exit
                    return determine_direction(position, new_position)
                queue.append(new_position)

    return "No valid moves"


def decide_move(partial_map):
    position = (1, 1)  # Bot is always at the center
    paths = []

    for index, row in enumerate(partial_map):
        for j, cell in enumerate(row):
            if cell == "-":
                paths.append((index, j, 0))  # Open path
            elif cell == "e":
                paths.append((index, j, 1))  # Exit

    # Prioritize paths by weight (exits first)
    paths.sort(key=lambda x: -x[2])

    # Determine valid moves from the adjacency map
    for path in paths:
        vertical_move = path[0] - position[0]
        horizontal_move = path[1] - position[1]

        if abs(vertical_move) + abs(horizontal_move) == 1:
            return determine_direction(position, path)

    return "No valid moves"


if __name__ == "__main__":
    _player_id = 1
    _partial_map = ["#-#", "#--", "-e-"]
    decide_move(_partial_map)

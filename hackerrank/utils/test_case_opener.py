from typing import List, Tuple


def test_case_opener(filename: str) -> Tuple[List[str], List[str]]:
    with open(filename, "r") as file:
        file.readline()
        list1 = file.readline().split()
        list2 = file.readline().split()

    return list1, list2


def test_case_opener_array_sum(filename: str) -> Tuple[int, int, List[List[int]]]:
    with open(filename, "r") as file:
        n, m = map(int, file.readline().split())
        result = [list(map(int, file.readline().split())) for _ in range(m)]

    return n, result

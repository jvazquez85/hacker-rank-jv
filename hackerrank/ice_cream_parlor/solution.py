from typing import List


def what_flavor(cost: List[int], money: int) -> None:
    price_map = {}

    for i, price in enumerate(cost):
        complement = money - price

        if complement in price_map:
            first_id = price_map[complement] + 1
            second_id = i + 1
            if first_id < second_id:
                print(first_id, second_id)
            else:
                print(second_id, first_id)
        price_map[price] = i

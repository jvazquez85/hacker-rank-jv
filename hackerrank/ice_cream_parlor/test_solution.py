import unittest

from hackerrank.ice_cream_parlor.solution import what_flavor


class TestSolution(unittest.TestCase):
    def test_first_test_case(self):
        cost = [2, 1, 3, 5, 6]
        money = 5
        self.assertEqual("1 3", what_flavor(cost, money))


if __name__ == "__main__":
    unittest.main()

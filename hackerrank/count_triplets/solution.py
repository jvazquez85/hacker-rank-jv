from collections import defaultdict
from typing import List


def count_triplets(arr: List[int], r: int) -> int:
    potential_twos = defaultdict(int)
    potential_threes = defaultdict(int)
    count = 0

    for number in arr:
        if number in potential_threes:
            count += potential_threes[number]

        if number in potential_twos:
            potential_threes[number * r] += potential_twos[number]

        potential_twos[number * r] += 1

    return count

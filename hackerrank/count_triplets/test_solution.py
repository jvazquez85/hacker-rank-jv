import unittest
from typing import List

from hackerrank.count_triplets.solution import count_triplets


class TestGeometricProgression(unittest.TestCase):
    def test_simple(self):
        arr: List[int] = [1, 3, 9, 9, 27, 81]
        ratio: int = 3
        expected: int = 6
        result = count_triplets(arr, ratio)
        self.assertEqual(expected, result)

import unittest
from typing import List

from hackerrank.count_inversions.solution import count_inversions


class TestSolution(unittest.TestCase):
    def test_simple(self):
        arr: List[int] = [1, 1, 1, 2, 2]
        expected = 0
        result = count_inversions(arr=arr)
        self.assertEqual(expected, result)
        expected = 4
        arr = [2, 1, 3, 1, 2]
        result = count_inversions(arr=arr)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

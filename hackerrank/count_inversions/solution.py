from typing import List


def count_inversions(arr: List[int]) -> int:
    if len(arr) <= 1:
        return 0

    mid = len(arr) // 2
    left_half = arr[:mid]
    right_half = arr[mid:]

    left_count = count_inversions(left_half)
    right_count = count_inversions(right_half)

    merge_count = merge_and_count(arr, left_half, right_half)

    return left_count + right_count + merge_count


def merge_and_count(arr: List[int], left_half: List[int], right_half: List[int]) -> int:
    i = j = k = 0
    swap_count = 0

    while i < len(left_half) and j < len(right_half):
        if left_half[i] <= right_half[j]:
            arr[k] = left_half[i]
            i += 1
        else:
            arr[k] = right_half[j]
            j += 1
            swap_count += len(left_half) - i
        k += 1

    while i < len(left_half):
        arr[k] = left_half[i]
        i += 1
        k += 1

    while j < len(right_half):
        arr[k] = right_half[j]
        j += 1
        k += 1

    return swap_count

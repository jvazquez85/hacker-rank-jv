from typing import List


def hourglassSum(arr: List[List]):
    last_sum = float("-inf")
    row = 0
    while row < 4:
        column = 0
        while column < 4:
            current_sum = (
                sum(arr[row][column : column + 3])
                + arr[row + 1][column + 1]
                + sum(arr[row + 2][column : column + 3])
            )
            last_sum = max(last_sum, current_sum)
            column += 1
        row += 1
    return last_sum

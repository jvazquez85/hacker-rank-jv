import unittest

from hackerrank.hourglass.app import hourglassSum


class TestHourGlass(unittest.TestCase):
    def test_case_one(self):
        result = hourglassSum(
            [
                [1, 1, 1, 0, 0, 0],
                [0, 1, 0, 0, 0, 0],
                [1, 1, 1, 0, 0, 0],
                [0, 0, 2, 4, 4, 0],
                [0, 0, 0, 2, 0, 0],
                [0, 0, 1, 2, 4, 0],
            ]
        )
        expected = 19
        self.assertEqual(expected, result, f"Expected {expected}, got {result}")


if __name__ == "__main__":
    unittest.main()

package hackerland

import (
	"sort"
)

type expenditure []int32

func (movements expenditure) Len() int {
	return len(movements)
}

func (movements expenditure) Less(i, j int) bool {
	return movements[i] < movements[j]
}

func (movements expenditure) Swap(i, j int) {
	movements[i], movements[j] = movements[j], movements[i]
}

func isOdd(number int) bool {
	return number%2 == 0
}

func CalculateMedian(movements expenditure) float32 {
	newArray := make(expenditure, len(movements))
	copy(newArray, movements)
	sort.Sort(newArray)
	middleNumber := len(newArray) / 2

	if isOdd(len(newArray)) {
		return (float32(newArray[middleNumber-1]) + float32(newArray[middleNumber])) / 2
	}
	return float32(newArray[middleNumber])
}

func ActivityNotifications(movements expenditure, days int32) int32 {
	var i, maxLoops, notifications int32
	var median float32
	maxLoops = int32(len(movements))
	for i = 0; i < maxLoops; i++ {
		if i+1 > days {
			firstDay := i - days
			median = CalculateMedian(movements[firstDay:i])
			if float32(movements[i]) >= median*2 {
				notifications += 1
			}
		}
	}

	return notifications
}

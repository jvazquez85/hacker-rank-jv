package hackerland

import (
	"fmt"
	"gitlab.com/jvazquez85/hacker-rank-jv/pkg"
	"strings"
	"testing"
)

func TestCalculateMedian(t *testing.T) {
	var expected float32
	sample := []int32{10, 20, 30}
	result := CalculateMedian(sample)
	expected = 20
	if result != expected {
		t.Fatalf("Median should be %f. Obtained %f", expected, result)
	}
	result = CalculateMedian([]int32{20, 30, 40})
	expected = 30
	if result != expected {
		t.Fatalf("Median should be %f. Obtained %f", expected, result)
	}
}

func TestExampleCase(t *testing.T) {
	var days, expectedNotifications int32
	days = 3
	notifications := ActivityNotifications([]int32{10, 20, 30, 40, 50}, days)
	expectedNotifications = 1
	if notifications != expectedNotifications {
		t.Fatalf("We expect %d notifications.Obtained %d\n", expectedNotifications, notifications)
	}
}

func TestExampleCaseOne(t *testing.T) {
	var days, expectedNotifications int32
	days = 5
	notifications := ActivityNotifications([]int32{2, 3, 4, 2, 3, 6, 8, 4, 5}, days)
	expectedNotifications = 2
	if notifications != expectedNotifications {
		t.Fatalf("We expect %d notifications.Obtained %d\n", expectedNotifications, notifications)
	}
}

func TestSampleTestCaseOne(t *testing.T) {
	var days, expectedNotifications int32
	days = 4
	notifications := ActivityNotifications([]int32{1, 2, 3, 4, 4}, days)
	expectedNotifications = 0
	if notifications != expectedNotifications {
		t.Fatalf("We expect %d notifications.Obtained %d\n", expectedNotifications, notifications)
	}
}

func TestCaseOne(t *testing.T) {
	reader := pkg.ObtainReader("fixtures/test-case-one.txt")
	movements := strings.Split(strings.TrimSpace(pkg.ReadLine(reader)), " ")
	fmt.Printf("It has %d elements\n", len(movements))
	//	var days, expectedNotifications int32
	//	days = 10000
	// notifications := ActivityNotifications([]int32{1, 2, 3, 4, 4}, days)
	//expectedNotifications = 633
	//if notifications != expectedNotifications {
	//	t.Fatalf("We expect %d notifications.Obtained %d\n", expectedNotifications, notifications)
	//}
}

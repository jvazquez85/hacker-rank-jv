import os
import unittest

from hackerrank.array_manipulation.app import array_manipulation
from hackerrank.utils.test_case_opener import test_case_opener_array_sum


class ArrayManipulationTestCase(unittest.TestCase):
    def test_base_case(self):
        expected = 200
        result = array_manipulation(5, [[1, 2, 100], [2, 5, 100], [3, 4, 100]])
        self.assertEqual(
            expected, result, f"Expected {expected}, but obtained {result}"
        )  # add assertion here

    def test_first_case(self):
        expected = 10
        result = array_manipulation(10, [[1, 5, 3], [4, 8, 7], [6, 9, 1]])
        self.assertEqual(
            expected, result, f"Expected {expected}, but obtained {result}"
        )  # add assertion here

    def test_seventh_case(self):
        sample_path = os.path.join(os.path.dirname(__file__), "test_case_seven.md")
        expected = 2497169732
        restum = test_case_opener_array_sum(sample_path)
        n, queries = restum[0], restum[1]
        result = array_manipulation(n=n, queries=queries)
        self.assertEqual(
            expected, result, f"Expected {expected}, but obtained {result}"
        )  # add assertion here


if __name__ == "__main__":
    unittest.main()

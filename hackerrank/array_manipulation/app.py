from typing import List


def array_manipulation(n: int, queries: List[List]):
    arr = [0] * (n + 1)
    for query in queries:
        a, b, k = query
        arr[a - 1] += k
        arr[b] -= k

    max_value = 0
    current_sum = 0

    for value in arr:
        current_sum += value
        max_value = max(max_value, current_sum)

    return max_value

    # base_array = [0] * (n + 1)
    # for operation in queries:
    #     a, b, k = operation[0], operation[1], operation[2]
    #     base_array[a - 1] += k
    #     if b < n:
    #         base_array[b] -= k
    #
    # for i in range(1, n):
    #     base_array[i] += base_array[i - 1]
    # return max(base_array[:-1])

class Solution:
    def reverse(self, x: int) -> int:
        min_int, max_int = -(2**31), 2**31 - 1
        reversed_num = 0
        sign = -1 if x < 0 else 1
        x = abs(x)

        while x != 0:
            digit = x % 10
            x //= 10
            reversed_num = reversed_num * 10 + digit

        reversed_num *= sign
        if reversed_num > max_int or reversed_num < min_int:
            return 0
        return reversed_num

def alternating_characters(s: str) -> int:
    string_length = len(s) - 1
    return sum(1 if s[char] == s[char + 1] else 0 for char in range(string_length))

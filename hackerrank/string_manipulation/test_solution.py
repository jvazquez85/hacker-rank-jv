import unittest

from hackerrank.string_manipulation.solution import alternating_characters


class MyTestCase(unittest.TestCase):
    def test_simple(self):
        sample = "AABAAB"
        expected = 2
        result = alternating_characters(sample)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()

package equal_stacks

import (
	"gitlab.com/jvazquez85/hacker-rank-jv/pkg"
	"io"
	"strings"
)

type EqualStackReader struct {
	pointer io.Reader
}

func (esr *EqualStackReader) FixtureReader(testCase string) {
	reader := pkg.ObtainReader(testCase)
	firstMultipleInput := strings.Split(strings.TrimSpace(readLine(reader)), " ")
}

import unittest

from hackerrank.minimum_swap.app import minimumSwaps


class TestMinimumSwap(unittest.TestCase):
    def test_minimumSwapCase0(self):
        arr = [7, 1, 3, 2, 4, 5, 6]
        result = minimumSwaps(arr)
        expected = 5
        self.assertEqual(expected, result, f"Expected {expected} but got {result}")


if __name__ == "__main__":
    unittest.main()

from typing import List


def minimumSwaps(arr: List[int]):
    swap_count = 0
    for current_position in range(len(arr)):
        number = arr[current_position]
        while number != current_position + 1:
            arr[current_position], arr[number - 1] = (
                arr[number - 1],
                arr[current_position],
            )
            swap_count += 1
            number = arr[current_position]  # Update the number after the swap

    return swap_count

package main

import (
	"bufio"
	"io"
	"os"
	"strconv"
	"strings"
)

const MaxShifts = 2
const ErrorValue = -1

type LineScanner struct {
	TestCases    map[int][]int32
	TestCasePath string
}

type Callable func(line []int32) int32

func (lp LineScanner) GetPeopleLine(peopleInLine int, lineOfPeople []string) []int32 {
	var peopleLine []int32
	for i := 0; i < peopleInLine; i++ {
		qItemTemp, err := strconv.ParseInt(lineOfPeople[i], 10, 64)
		checkError(err)
		person := int32(qItemTemp)
		peopleLine = append(peopleLine, person)
	}
	return peopleLine
}

func (lp *LineScanner) ReadFixture(testCasePath string) {
	var peopleInLine int
	fixture, error := os.Open(testCasePath)
	defer fixture.Close()
	checkError(error)
	reader := bufio.NewReaderSize(fixture, 1024*1024)
	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	fixtureLines := int32(tTemp)
	lp.TestCases = make(map[int][]int32)
	for tItr := 0; tItr < int(fixtureLines); tItr++ {
		literalPeopleInLine, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		lineOfPeople := strings.Split(readLine(reader), " ")
		peopleInLine = int(literalPeopleInLine)
		lp.TestCases[tItr] = lp.GetPeopleLine(peopleInLine, lineOfPeople)
	}
}

func Max(maxValue int32, minValue int32) int32 {
	if maxValue < minValue {
		return minValue
	} else {
		return maxValue
	}
}

// MinimumBribes solve the hackerrank program. It will execute different functions
func (lp *LineScanner) MinimumBribes(consumerLine []int32, callable Callable) int32 {
	return callable(consumerLine)
}

// IsError Person should not have a difference with the position
// of more than two spots.
func IsError(person int32, spot int32) bool {
	if (person - spot) > MaxShifts {
		return true
	}
	return false
}

// PersonAheadOfMeBrivedMe O(1) operation that determines if the first parameter, currentPerson is smaller
// than the second parameter.
func PersonAheadOfMeBrivedMe(currentPerson int32, personAhead int32) bool {
	if currentPerson < personAhead {
		return true
	}
	return false
}

// ScanAhead old implementation that i dont' recall what it does
func ScanAhead(consumerLine []int32) int32 {
	var positionShifts, currentFixedPosition int32

	for currentPosition, currentPerson := range consumerLine {
		currentFixedPosition = int32(currentPosition) + 1
		if (currentPerson - currentFixedPosition) > MaxShifts {
			return ErrorValue
		}

		//logMessage(fmt.Sprintf("%d - %d = %d", currentFixedPosition, currentPerson, manualDelta), verbose)
		manualDelta := currentFixedPosition - currentPerson
		switch {
		case manualDelta > 1:
			positionShifts += manualDelta
		//case manualDelta < 1:
		//	logMessage(fmt.Sprintf("+ %d", manualDelta * -1), verbose)
		//	positionShifts += manualDelta * -1
		default:
			deltaIndex := Max(currentFixedPosition-3, 0)
			lineAhead := consumerLine[deltaIndex : currentFixedPosition-1]
			sliceAhead(lineAhead, currentPerson, &positionShifts)
		}
	}
	return positionShifts
}

func sliceAhead(lineAhead []int32, currentPerson int32, positionShifts *int32) {
	for _, personAhead := range lineAhead {
		if personAhead > currentPerson {
			*positionShifts += 1
		}
	}
}

func main() {

}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
